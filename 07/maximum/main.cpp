// maximize

// trick: substitute z^4 with z^2
// trick: solve_nonnegative_quadratic_program gives only positive solutions, somehow enforcing the min bounds with constraints didnt work. for arbitrary solutions use solve_quadratic_program
// track: easy floor to ceil -> ceil(x) = -floor(-x)
#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpq.h>

// choose exact integral type
typedef CGAL::Gmpq ET;

// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

using namespace std;

double floor_to_double(const CGAL::Quotient<ET> x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

int main() {
	int p,a,b; cin >>p;
	while(p!=0) {
		cin>>a>>b;
		
		const int x = 0; 
		const int y = 1; 
		const int z = 2; 
		
		Program qp (CGAL::SMALLER, false, 0, false, 0);
		
		if(p==1) {
			
			// constraints
		
			qp.set_a(x, 0, -1); qp.set_b(0, 0);
			qp.set_a(y, 1, -1); qp.set_b(1, 0);
			qp.set_a(x, 2, 1); qp.set_a(y, 2, 1); qp.set_b(2, 4);
			qp.set_a(x, 3, 4); qp.set_a(y, 3, 2); qp.set_b(3, a*b);
			qp.set_a(x, 4, -1); qp.set_a(y, 4, 1); qp.set_b(4, 1);
			
			// minimizer stuff
			qp.set_d(x, x, 2*ET(a));
			qp.set_c(y,-b); 
			
			// die assertions gend be mir nur shit uus bem quadratic program...
			//assert(qp.is_nonnegative());
			Solution s = CGAL::solve_quadratic_program(qp, ET());
			//assert (s.solves_quadratic_program(qp));
			
			if (s.status() == CGAL::QP_INFEASIBLE) cout << "no\n";
			else if(!s.is_optimal()) cout << "unbounded\n";
			else {
				CGAL::Quadratic_program_solution<ET>::Variable_value_iterator opt = s.variable_values_begin();
				cout << int(floor_to_double(-s.objective_value())) << "\n";
			}
		}
		else if(p==2) {
			
			// constraints
			qp.set_a(x, 0, 1); qp.set_b(0, 0);
			qp.set_a(y, 1, 1); qp.set_b(1, 0);
			qp.set_a(x, 2, -1); qp.set_a(y, 2, -1); qp.set_b(2, 4	);
			qp.set_a(x, 3, -4); qp.set_a(y, 3, -2); qp.set_a(z, 3, -1); qp.set_b(3, a*b);
			qp.set_a(x, 4, 1); qp.set_a(y, 4, -1); qp.set_b(4, 1);
			
			// minimizer stuff
			qp.set_d(x, x, 2*ET(a));
			qp.set_d(z, z, 2);
			qp.set_c(y,b); 
			
			Solution s = CGAL::solve_quadratic_program(qp, ET());
			//assert (s.solves_quadratic_program(qp));
			
			if (s.status() == CGAL::QP_INFEASIBLE) cout << "no\n";
			else if(!s.is_optimal()) cout << "unbounded\n";
			else {
				CGAL::Quadratic_program_solution<ET>::Variable_value_iterator opt = s.variable_values_begin();
				cout << int(-floor_to_double(-s.objective_value())) << "\n";
			}
		}
		cin>>p;
	}
	
	return 0;
}
