// portfolio

// grosse scheiss: bem setze vo D muess j<=i sii!!!! nur underes drüüeck setze...WTF... und ned vergesse mer muess 2*D inefüege... wtf CGAL 

#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpzf.h>

using namespace std;

// choose exact floating-point type
typedef CGAL::Gmpq ET;

// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main() {
	int n,m; cin>>n>>m;
	while(n!=0 || m != 0) {
		Program qp (CGAL::SMALLER, false, 0, false, 0);
		int c,r;
		for(int i=0;i<n;i++) {
			cin>>c>>r;
			qp.set_a(i, 0, c);
			qp.set_a(i, 1, -r);
		}
		
		int cov;
		for(int i=0;i<n;i++) {
			for(int j=0;j<n;j++) {
				cin>>cov;
				if(j<=i) qp.set_d(i, j, 2*cov); 
			}
		}
		
		int C,R,V;
		for(int i=0;i<m;i++) {
			cin>>C>>R>>V;
			qp.set_b(0, C);
			qp.set_b(1, -R); 
			
			Solution s = CGAL::solve_nonnegative_quadratic_program(qp, ET());
			
			if (s.status() == CGAL::QP_INFEASIBLE) cout << "No.\n";
			else if(s.objective_value() <= V) cout << "Yes.\n";
			else cout << "No.\n";
		}
		cin>>n>>m;
	}
	return 0;
}
