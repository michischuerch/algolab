// inball

// idea: we have one equation to fulfil for each cave wall:
// |a'*m - b| / ||a|| >= r
// this means the radius r is smaller or equal to all distances of the midpoint f the circle to the wall
// i got the distance function from wikipedia "distance point / line"
// we then also know that a'*m - b <= 0
// therefore 
// |a'*m - b| = b - a'*m
// note also that we dont enforce "a'*m - b <= 0" because we want to see if the radius is negativ -> "none" or the radius doesnt converge -> "inf"

#include <iostream>
#include <vector>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>

// choose exact integral type
typedef CGAL::Gmpz ET;

// program and solution types
typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

using namespace std;

double floor_to_double(CGAL::Quotient<ET> x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

int main() {
	int n,d;cin>>n;
	while(n!=0) {
		// we have d+1 variables: first is radius, then for the midpoint of the circle
		cin>>d;
		const int radius = d;
		Program lp (CGAL::SMALLER, false, 0, false, 0);
		
		vector<int> a(d);
		int b;
		for(int i=0;i<n;i++) {
			for(int j=0;j<d;j++) cin>>a[j];
			cin>>b;
			
			int norm = 0;
			for(int j=0;j<d;j++) norm+=a[j]*a[j];
			norm = sqrt(norm);
			
			for(int j=0;j<d;j++) lp.set_a (j, i, a[j]);
			lp.set_a (radius, i, norm);
			lp.set_b (i, b);
			
		}
		lp.set_c(radius, -1);
		
		Solution s = CGAL::solve_linear_program(lp, ET());
		assert (s.solves_linear_program(lp));
		
		if(!s.is_optimal()) cout << "inf\n";
		else if (s.is_optimal() && (s.objective_value() <= 0)) {
			CGAL::Quadratic_program_solution<ET>::Variable_value_iterator
			opt = s.variable_values_begin();
			cout << int(floor_to_double(*(opt+d))) << "\n";
		} else cout << "none\n";
		cin>>n;
	}
	return 0;
}
