// diet

// example: decide whether there exists a disk that covers a given set
// of points R in the plane and is disjoint from another set of points B
#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>

// choose exact integral type
typedef CGAL::Gmpz ET;

// program and solution types
typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;
//typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;


using namespace std;

double floor_to_double(const CGAL::Quotient<ET> x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

int main() {
	int n,m; cin>>n>>m;
	while(n!=0 || m != 0) {
		Program lp (CGAL::SMALLER, true, 0, false, 0);
		int minI,maxI;
		for(int i=0;i<n;i++) {
			cin>>minI>>maxI;
			lp.set_b (i, maxI);
			lp.set_b (n+i, -minI);
		}
		vector<int> prices(m);
		for(int j=0;j<m;j++) {
			cin >> prices[j];
			lp.set_c(j,prices[j]);
			int nutrient;
			for(int i=0;i<n;i++) {
				cin>>nutrient;
				lp.set_a (j,i, nutrient);
				lp.set_a (j,n+i, -nutrient);
			}
		}
		
		// solve the program, using ET as the exact type
		Solution s = CGAL::solve_linear_program(lp, ET());
		assert (s.solves_linear_program(lp));
		
		if (s.status() != CGAL::QP_INFEASIBLE) {
			CGAL::Quadratic_program_solution<ET>::Variable_value_iterator
			  opt = s.variable_values_begin();
			CGAL::Quotient<ET> cost=0;
			for(int j=0;j<m;j++) cost += prices[j] * *(opt+j);
			std::cout << int(floor_to_double(cost));
		} else std::cout << "No such diet.";
		std::cout << "\n";
			
		cin>>n>>m;
	}
	return 0;
}
