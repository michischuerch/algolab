#include <iostream>

using namespace std;

int main() {
	int n,t,b,b2,solution,toFall;
	bool ended;
	cin >> t;
	for(int tt=0; tt<t; tt++) {
		cin >> n;
		solution = 0;
		toFall = 0;
		ended = false;
		for(int i=0;i<n;i++) {
			cin >> b;
			if(!ended) {
				if(b > toFall) toFall = b;
			
				toFall--;
				solution++;
				if(toFall == 0) ended = true;
				/*if(toFall >= n-i) {
					solution = n;
					ended = true;
				}*/
			}
		}
		cout << solution << "\n";
	}
	return 0;
}
