#include <iostream>

using namespace std;

/*void print_array(bool *a){
    for(int i = 0; i<(sizeof(a)/sizeof(*a));i++) {
	cout << a[i] << " ";
    }
    cout << endl;

}*/

int main(){
    ios_base::sync_with_stdio(false);
    int t,n,k,ic;
    cin >> t;
    for(int i = 0; i < t; i++){
		cin >> n >> k;
		bool result[n] = {};
		int smaller[n] = {};
		int bigger[n] = {};
		fill_n(result,n,1);
		ic = 0;
    	for(int j=0;j<k;j++){
	    	int pi;
	    	cin >> pi;
	    	int left[pi],right[pi]; 
	    	int ci;
	    	for(int jj=0;jj<pi;jj++){
				cin >> ci;
				left[jj] = ci;
	    	}
	    	for(int jj=0;jj<pi; jj++){
				cin >> ci;
				right[jj] = ci;
	    	}
	    	char sign;
	    	cin >> sign;
	    	if(sign == '>'){
				for(int ii=0;ii<pi;ii++){
		    		smaller[right[ii]-1]++;
		    		bigger[left[ii]-1]++;
				}
				ic++;
	    	}
	    	if(sign == '<'){
				for(int ii=0;ii<pi;ii++){
		    		smaller[left[ii]-1]++;
		    		bigger[right[ii]-1]++;
				}
				ic++;
	    	}
	    	if(sign == '='){
				for(int ii=0;ii<pi;ii++){
		    		result[left[ii]-1] = 0;
		    		result[right[ii]-1] = 0;
				}
	    	}
		}
		if(ic) {
			for(int k=0;k<n;k++){
	    		if(!(bigger[k] | smaller[k])){
					result[k] = 0;
	    		}
			}
    		for(int k=0;k<n;k++){
				if((bigger[k] == ic & smaller[k] == ic) | (bigger[k] != ic & smaller[k] != ic)){
	    			result[k] = 0;
	    		}
			}
		}
		int solution = -1;
		for(int ii=0;ii<n;ii++){
   	    	if(result[ii]){
				if(solution != -1) {
		    		solution = 0;
		    		break;
				}
				solution=ii+1;
	    	}
		}
		cout << solution << endl;
		/*cout << endl;
	for(int kk=0;kk<n;kk++){
    	    cout << result[kk] << " ";
	}
	cout << endl;
	
	for(int kk=0;kk<n;kk++){
	    cout << bigger[kk] << " ";	
	}

	cout << endl;

	for(int kk=0; kk<n;kk++){
	    cout << smaller[kk] << " ";
	}
	cout << endl;*/
    }
    
    return 0;
}
