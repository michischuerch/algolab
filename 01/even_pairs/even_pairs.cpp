#include <iostream>
#include <vector>

using namespace std;

int factorial(int n){
	if(n<=1) return 1;
	
	int solution = 1;
	for(int i=2;i<=n;i++) {
		solution *= i;
	}
	
	return solution;
}

int binomial_coeff(int n,int k){

	int solution = 1;
	for(int i=n;i>n-k;i--) {
		solution *= i;
	}

	return solution/factorial(k);
}

int main(){
	int t, n, solution,sum,x,even,odd;

	/*cout << binomial_coeff(5,2);
	return 0;*/

	cin >> t;
	for(int tt=0; tt<t; tt++){
		cin >> n; 
		solution=0;
		even=0;
		odd=0;
		sum = 0;
		for(int nn=0;nn<n;nn++){
			cin >> x;
			
			sum += x;
			if(sum%2==0) even++;
			else odd++;
		}	
		solution = even + binomial_coeff(even,2) + binomial_coeff(odd,2);
		/*for(int i=0;i<n;i++) {
			for(int j=0;j<n-i;j++){
				data[j] = (data[j] & data[j+1]) | (!data[j] & !data[j+1]);
				solution += data[j];
			}
		}*/

		//for(int i=1;i<ones;i*=2){
		//	solution+=(ones-i)/i;
		//}
		
		cout << solution << "\n";

	}

	return 0;
}
