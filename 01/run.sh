#!/bin/bash
cd $1
g++ -Wall -o $1 $1.cpp
for f in testsets/*.in; do 
	echo $f
	./$1 < $f
	echo
done
rm $1
cd ..
