#include <iostream>
#include <vector>

using namespace std;

int main(){
	int t,n;
	float sum,x;
	cin >> t;
	for(int i=0; i<t;i++){
		sum=0;
		cin >> n;
		for(int j=0; j<n;j++){
			cin >> x;
			sum+=x;
		}
		cout << sum << "\n";
	}
	return 0;
}
