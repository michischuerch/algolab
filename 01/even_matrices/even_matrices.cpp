#include <iostream>
#include <vector>

using namespace std;

int factorial(int n){
	if(n <= 1) return n;

	int solution = 1;
	for(int i=2;i<=n;i++) {
		solution *=i;
	}

	return solution;
}

int binomial_coefficient(int n, int k){
	int solution = 1;
	for(int i=n; i > n-k; i--) {
		solution *=i;
	}

	return solution / factorial(k);
}

int countEE_OO(vector<vector<int>> data,int nn,int n){
	
    int even, odd, result = 0;
	for(int i=0;i<nn;i++) {
		// count number of Es / Os in row
		
		even = 0;
		odd = 0;
		for(int j=0;j<n;j++) {
			if(data[i][j]%2==0) {
				even++;
			} else {
				odd++;
			}
		}

		result += binomial_coefficient(even,2) + binomial_coefficient(odd,2) + even;
	}

	return result;
}


int main(){
	/*cout << binomial_coefficient(2,2);
	cout << factorial(2);
	return 0;*/
	
	int t,n,x,solution;
	cin >> t;
	for(int tt=0; tt < t; tt++) {
		solution = 0;
		
		cin >> n;
		
		int data[n][n];
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				cin >> x;
				data[i][j] = x;
			}
		}		
		
/*
		vector<vector<int> > ps(n);
		for(int i=0;i<n;i++) ps[i].resize(n);
		for(int i=0;i<n;i++) {
			for(int j=0;j<n;j++){
				ps[i][j] = data[i][j];
			}
		}
		for(int i=0;i<n;i++) {
			for(int j=1;j<n;j++) {
				ps[i][j] = ps[i][j-1]+ps[i][j];
			}
		}

		vector<vector<int> > ps2(n);
		for(int i=0;i<n;i++) ps2[i].resize(n);

		for(int nn=0;nn<n;nn++){
			
			for(int i=0;i<n-nn;i++) {
				for(int j=0;j<n;j++){
					ps2[i][j] += ps[i+nn][j];
				}
			}

			solution += countEE_OO(ps2,n,nn);
			cout << solution << " ";		
		}
*/

		for(int nn=1; nn<=n; nn++) {
			vector<vector<int> > ps(nn);
			for(int i=0;i<nn;i++) ps[i].resize(n);

			// fill ps

			//cout << "ps before:";
			for(int i=0;i<nn;i++){
				for(int j=0;j<n;j++){
					ps[i][j] = data[n-nn+i][j];
					//cout << ps[i][j] << " ";
				}
				//cout << endl;
			}

			// calc difference matrix

			//cout << "ps" << endl;
			for(int j=1;j<n;j++){
				ps[0][j] = ps[0][j-1]+ps[0][j];
				//cout << ps[0][j] << " ";
			}
			//cout << endl;
			//cout << "ps: nn" << nn << endl;
			int sum = 0;
			for(int i=1;i<nn;i++) {
				sum = 0;
				for(int j=0;j<n;j++){
					sum+=ps[i][j];
					ps[i][j] = sum + ps[i-1][j];
					//cout << ps[i][j] << " ";
				}
				//cout << endl;
			} 
			/*for (auto i = ps.begin(); i != ps.end(); ++i){
			  for(auto j = i.begin(); j != i.end(); ++j)
			  cout << *j << ' ';
			  cout << endl;
			  }*/
			
			solution += countEE_OO(ps,nn,n);
			//cout << solution << " ";

		}
		
		cout << solution << "\n";
		/*int ps[n][n];
		int sum = 0;
		for(int i=0;i<n;i++){
			sum+=data[0][i];
			ps[0][i] = sum;
		}*/

		/*for(int i=1;i<n;i++){
			sum=0;
			for(int j=0;j<n;j++){
				sum+=data[i][j];
				ps[i][j]=sum+data[i-1][j];
			}
		}*/

		

	}
	return 0;
}
