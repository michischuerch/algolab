#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <tuple>

using namespace std;

int main() {
	ios_base::sync_with_stdio(false);
	int t,n,len,pos,maxLen;
	cin >> t;
	while(t--) {
		// data
		cin >> n;
		vector<tuple<int,int,int> > maxPos_pos_lens;
		vector<pair<int,int> > pos_lens;
		for(int i=0;i<n;i++) {
			cin >> len >> pos;
			pos_lens.push_back(make_pair(pos,len));
		}
		sort(pos_lens.begin(),pos_lens.end());
		int solution=1;
		// algo
		int nb = 1;
		for(int left=pos_lens[0].first;nb < n;) {
			if(pos_lens[nb].first < left) {
				nb++;
				continue;
			}
			int upperBound = max(left + pos_lens[nb].second, pos_lens[nb].first);
			for(int check = nb;check < n && upperBound > pos_lens[check].first;check++) {
				int nextBound = max(left + pos_lens[check].second, pos_lens[check].first);
				if(nextBound < upperBound) {
					upperBound = nextBound;
					nb = check;
				}
			}
			left = upperBound;
			solution++;
			nb++;
		}
		cout << solution << "\n";
	}
	return 0;
}
