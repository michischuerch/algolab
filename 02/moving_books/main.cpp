// moving books 2

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

void testcases() {
	int n,m; cin>>n>>m;
	
	vector<int> ss(n);
	vector<int> ws(m);
	for(int i=0;i<n;i++) cin>>ss[i];
	for(int i=0;i<m;i++) cin>>ws[i];
	
	sort(ss.begin(),ss.end());
	sort(ws.begin(),ws.end());
	
	if(ss[n-1] < ws[m-1]) {
		cout << "impossible\n";
		return;
	}
	
	int active=1;
	int time = 0;
	for(int i=0;i<m;) {
		time++;
		i+=active;
		while(i<m && ss[n-active-1] >= ws.at(m-i-1)) {
			active++;
			i+=time;
		}
		
	}
	
	cout << time*3-1;
	cout << "\n";
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false);
	int T;	cin >> T;	// First input line: Number of testcases.
	while(T--)	testcases();
	return 0;
}
