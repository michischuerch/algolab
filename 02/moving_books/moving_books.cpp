#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	ios_base::sync_with_stdio(false);
	int t;
	cin >> t;
	while(t--) {
		// data
		int n,m;
		cin >> n >> m;
		vector<int> friends(n);
		vector<int> boxes(m);
		for(int i=0;i<n;i++) cin >> friends[i];
		for(int i=0;i<m;i++) cin >> boxes[i];
		sort(friends.begin(), friends.end(), greater<int>());
		sort(boxes.begin(), boxes.end(), greater<int>());
		// check for possibility
		if(friends[0] < boxes[0]) {
			cout << "impossible\n";
			continue;
		}
		// algo
		int time = 0;
		int friendsActive = 1;
		for(int boxesDelivered=0; boxesDelivered<m;) {
			// look how much friends can carry the next box
			time++;
			boxesDelivered+=friendsActive;
			while(friendsActive < n && boxesDelivered < m && friends[friendsActive] >= boxes[boxesDelivered]) {
				friendsActive++;
				boxesDelivered+=time;
			}
		}
		cout << time*3-1 << "\n";
	}
	return 0;
}
