#include <iostream>
#include <vector>
//#include <utility>
#include <limits>
#include <algorithm>

using namespace std;

/*bool pairCompare(const pair<double, Processor*>& firstElem, const pair<double, Processor*>& secondElem) {
  return firstElem.first < secondElem.first;
}*/


int main(){
	ios_base::sync_with_stdio(false);
	int t,n,solution;
	
	cin >> t;
	while(t--){
		
		// input
		cin >> n;
		vector<int> m(n);
		for(int i=0;i<n;i++) {
			cin >> m[i];
		}
		
		vector<vector<int> > data(n);
		for(int i=0;i<n;i++) 
			data[i].resize(m[i]);
		
		for(int i=0;i<n;i++) {
			for(int j=0;j<m[i];j++) {
				cin >> data[i][j];
			}
		}
			
		// algo
		vector<int> p(n,0);
		vector<pair<int,int> > fastA(n);
		for(int i=0;i<n;i++) {
			fastA[i] = make_pair(data[i][0],i);
		}
		
		sort(fastA.begin(),fastA.end());
		
		// get a global sorted list
		
		bool done = false;
		solution = numeric_limits<int>::max();
		
		while(!done) {
			
			// get current window size
			int max = fastA.back().first;
			int min = fastA.front().first;
			/*int smallestIndex = 0;
			for(int i=0;i<n;i++) {
				int v = data[i][p[i]];
				if(v > max) max = v;
				if(v < min) {
					min = v;
					smallestIndex = i;
				}
			}*/
			
			
			
			
			
			if(solution > max - min) {
				solution = max - min;
			}
			
			//cout << max << " " << min << " " << solution << " | " ;
			
			if(p[fastA.front().second] == m[fastA.front().second]-1) done = true;
			else {
				p[fastA.front().second]++;
				pair<int,int> newPair = make_pair(data[fastA.front().second][p[fastA.front().second]],fastA.front().second);
				fastA.erase(fastA.begin());
				fastA.insert(upper_bound( fastA.begin(), fastA.end(), newPair),newPair);
			}
			
		}
		
		cout << solution+1;
		cout << "\n";
	}
	
	return 0;
}
