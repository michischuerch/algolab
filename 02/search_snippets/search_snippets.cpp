#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;


int main(){
	ios_base::sync_with_stdio(false);
	int t,n,solution;
	
	cin >> t;
	while(t--){
		
		// input
		cin >> n;
		vector<int> m(n);
		for(int i=0;i<n;i++) cin >> m[i];
		
		vector<vector<int> > data(n);
		for(int i=0;i<n;i++) 
			data[i].resize(m[i]);
		
		for(int i=0;i<n;i++) {
			for(int j=0;j<m[i];j++) {
				cin >> data[i][j];
			}
		}
		int sum = 0;
		for(int i=0;i<n;i++) sum+=m[i];
		
		// algo
		
		vector<int> p(n,0);
		
		vector<pair<int,int> > fastA(sum);
		int counter = 0;
		for(int i=0;i<n;i++) {
			for(int j=0;j<m[i];j++) {
				fastA[counter] = make_pair(data[i][j],i);
				counter++;
			}
		}
		// get a global sorted list
		sort(fastA.begin(),fastA.end());
		
		
		bool done = false;
		solution = numeric_limits<int>::max();
		
		int max = numeric_limits<int>::max();
		int min = fastA.front().first;
		
		int c = 0;
		
		int pointer = 0;
		for(int k=0;k<fastA.size();k++) {
			
			// get current window size
			if(solution > max - min) {
				solution = max - min;
			}
			
			if(p[fastA[k].second] == 0)	c++;
			
			p[fastA[k].second]++;
			
			// we can trow everything away that we have seen more than once from our sorted list
			while(p[fastA[pointer].second] > 1) {
				p[fastA[pointer].second]--;
				pointer++;
			}
			
			if(c == n) {
				max = fastA[k].first;
				min = fastA[pointer].first;
			}
		}
		
		cout << solution+1;
		cout << "\n";
	}
	
	return 0;
}
