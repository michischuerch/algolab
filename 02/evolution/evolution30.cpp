#include <iostream>
#include <vector>
#include <map>
#include <string>

using namespace std;

int main() {
	ios_base::sync_with_stdio(false);
	
	int t;
	cin >> t;
	while(t--) {
		
		// data
		int n,q;
		cin >> n >> q;
		
		map<string,int> wordNumMap;
		map<int,string> numWordMap;
		vector<pair<int,int> > numAgeVec;
		//vector<vector<int> > pathUp;
		vector<int> relations(n);
		
		for(int i=0;i<n;i++) {
			string name;
			int age;
			cin >> name >> age;
			wordNumMap[name] = i;
			numWordMap[i] = name;
			numAgeVec.push_back(make_pair(i,age));
		}
				
		for(int i=0;i<n-1;i++) {
			string a, b;
			cin >> a >> b;
			
			// find two integer values corresponding to a, b
			int pos1 = wordNumMap[a];
			int pos2 = wordNumMap[b];
			
			relations.at(pos1) = pos2;
		}
		//relations[] = posLUCA;
		
		
		// find luca and set the relation to himself
		int max=0;
		int posLUCA=0;
		for(int i=0;i<n;i++) {
			if(max < numAgeVec[i].second) {
				max = numAgeVec[i].second;
				posLUCA = numAgeVec[i].first; 
			}
		}
		relations[posLUCA] = posLUCA;
		
		// algo
		for(int i=0;i<q;i++) {
			string word;
			int age;
			cin >> word >> age;
			int pos = wordNumMap[word];
			int lastPos = -1;
			
			while(numAgeVec[pos].second <= age && lastPos != pos) {
				lastPos = pos;
				pos = relations[pos];
			}
			cout << numWordMap[lastPos];
			if(i == q-1) cout << "\n";
			else cout << " ";
			
		}
	}
	
	return 0;
}
