#include <iostream>
#include <vector>

using namespace std;

bool too_small(int i, vector<int> &a) {
	int check = 21;
	return a[i] < check;
}

int main() {
	vector<int> a;
	
	for(int i=0;i<137;i++) a.push_back(i);
	
	int lmin = 0, lmax = 1;
	//while (too_small(lmax,a)) lmax *= 2;
	lmax = a.size();
	
	
	while (lmin != lmax) {
		
		int p = (lmin + lmax)/2;
		if (too_small(p,a)) lmin = p + 1;
		else lmax = p;
		cout << p << endl;
	}
	
	cout << lmin;
}
