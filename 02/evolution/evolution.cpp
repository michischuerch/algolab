#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>

using namespace std;

void build_exponentialJump(int pos,vector<vector<int> > &exponentialJump,vector<vector<int> > &descendents,vector<int> &path) {
	// build list where we jump expnentially recursively
	path.push_back(pos);
	// build jump vector
	vector<int> jumpVec;
	for(int i=1;i<path.size();i*=2) jumpVec.push_back(path[path.size()-i]);
	// care that we need element 0! (i think at least, else we would not notice the bound)
	jumpVec.push_back(path.front());
	exponentialJump[pos] = jumpVec;
	for(int i=0;i<descendents[pos].size();i++) build_exponentialJump(descendents[pos][i],exponentialJump,descendents,path);
	path.pop_back();
}

int main() {
	ios_base::sync_with_stdio(false);
	int t;
	cin >> t;
	while(t--) {
		// data
		int n,q;
		cin >> n >> q;
		map<string,int> wordNumMap;
		map<int,string> numWordMap;
		vector<int> ageVec(n);
		vector<vector<int> > descendents(n);
		// get ages and namePosition maps
		for(int i=0;i<n;i++) {
			string name;
			int age;
			cin >> name >> age;
			wordNumMap[name] = i;
			numWordMap[i] = name;
			ageVec[i] = age;
		}
				
		for(int i=0;i<n-1;i++) {
			string a, b;
			cin >> a >> b;
			// find two integer values corresponding to a, b
			int pos1 = wordNumMap[a];
			int pos2 = wordNumMap[b];
			descendents[pos2].push_back(pos1);
		}
		// find luca and get maximum number of age
		int max=0;
		int posLUCA=0;
		for(int i=0;i<n;i++) 
			if(max < ageVec[i]) {
				max = ageVec[i];
				posLUCA = i; 
			}
		// dfs build exponential jump list
		vector<vector<int> > exponentialJump(n);
		vector<int> path;
		build_exponentialJump(posLUCA,exponentialJump,descendents,path);
		// algo
		for(int i=0;i<q;i++) {
			string word;
			int age;
			cin >> word >> age;
			int pos = wordNumMap[word];
			
			int solutionPos = -1;
			// special case where we would search for a element above root
			if(age >= max) solutionPos = posLUCA;
			while(solutionPos == -1)
				for(int j=1;j<exponentialJump[pos].size();j++)  
					if(age < ageVec[exponentialJump[pos][j]]) {
						if(j == 1) solutionPos = exponentialJump[pos][0];
						else pos = exponentialJump[pos][j-1];
						break;
					}
			cout << numWordMap[solutionPos];
			i == q-1 ? cout << "\n" : cout << " ";
		}
	}
	return 0;
}
