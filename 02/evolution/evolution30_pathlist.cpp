#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>

using namespace std;

int main() {
	ios_base::sync_with_stdio(false);
	
	int t;
	cin >> t;
	while(t--) {
		
		// data
		int n,q;
		cin >> n >> q;
		
		map<string,int> wordNumMap;
		map<int,string> numWordMap;
		vector<pair<int,int> > ageNumVec;
		vector<int> relations(n);
		
		for(int i=0;i<n;i++) {
			string name;
			int age;
			cin >> name >> age;
			wordNumMap[name] = i;
			numWordMap[i] = name;
			ageNumVec.push_back(make_pair(age,i));
		}
				
		for(int i=0;i<n-1;i++) {
			string a, b;
			cin >> a >> b;
			
			// find two integer values corresponding to a, b
			int pos1 = wordNumMap[a];
			int pos2 = wordNumMap[b];
			
			relations.at(pos1) = pos2;
		}
		
		
		// find luca and set the relation to himself
		int max=0;
		int posLUCA=0;
		for(int i=0;i<n;i++) {
			if(max < ageNumVec[i].first) {
				max = ageNumVec[i].first;
				posLUCA = ageNumVec[i].second; 
			}
		}
		relations[posLUCA] = posLUCA;
		
		vector<bool> isLeaf(n,1);
		// find all leaves
		for(int i=0;i<n;i++) {
			isLeaf[relations[i]] = 0;
		}
		
		vector<vector<pair<int,int> > > pathUp;
		vector<pair<int,int> > pathPos(n);
		// go through all leaves and build all paths up and save positions of each element in the path
		int pathCount = 0;
		for(int i=0;i<n;i++) {
			if(isLeaf[i]) {
				vector<pair<int,int> > newPath;
				
				int pos = i;
				int lastPos = -1;
				
				int positionCount = 0;
				while(lastPos != pos) {
					newPath.push_back(ageNumVec[pos]);
					pathPos[pos] = make_pair(pathCount,positionCount);
					lastPos = pos;
					pos = relations[pos];
					
					positionCount++;
				}
				pathUp.push_back(newPath);
				pathCount++;
			}
		}
		
		
		// algo
		for(int i=0;i<q;i++) {
			string word;
			int age;
			cin >> word >> age;
			int pos = wordNumMap[word];
			
			// get path and the position
			vector<pair<int,int> > path = pathUp[pathPos[pos].first];
			int pathLeft = pathPos[pos].second;
			
			int solutionPos = (*(upper_bound( path.begin()+pathLeft, path.end(), make_pair(age,age))-1)).second;
			
			cout << numWordMap[solutionPos];
			if(i == q-1) cout << "\n";
			else cout << " ";
			
		}
	}
	
	return 0;
}
