// marathon

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <map>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
// Namespaces
using namespace std;
using namespace boost;


// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor,
				property<edge_weight_t, int> > > > >	Graph;
// Interior Property Maps
typedef	property_map<Graph, edge_capacity_t>::type		EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type		ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	graph_traits<Graph>::edge_descriptor			Edge;
typedef property_map<Graph, edge_weight_t>::type	WeightMap;


// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;
	WeightMap	&weightmap;
public:
	
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap, WeightMap &weightmap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap), weightmap(weightmap){}

	// to use the Function (add an edge)
	void addSingleEdge(int from, int to, long capacity) {
		Edge e;
		bool success;
		tie(e, success) = add_edge(from, to, G);
		weightmap[e] = capacity;
	}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, reverseE;
		bool success;
		tie(e, success) = add_edge(from, to, G);
		tie(reverseE, success) = add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[reverseE] = 0;
		revedgemap[e] = reverseE;
		revedgemap[reverseE] = e;
	}
};

// Functions
// =========
// Function for an individual testcase
void testcases() {
	
	int n,m,s,f; cin>>n>>m>>s>>f; // #nodes, #edges, start, finish
	
	vector<vector<int>> data; // from, to, width, length
	for(int i=0;i<m;i++) {
		vector<int> v(4);
		cin>>v[0]>>v[1]>>v[2]>>v[3];
		data.push_back(v);
	}
	// Create Graph and Maps
	/*Graph G(n);
	EdgeCapacityMap weightmap = get(edge_capacity, G);*/
	
	Graph G(2*n+2*m);
	EdgeCapacityMap capacitymap = get(edge_capacity, G);
	ReverseEdgeMap revedgemap = get(edge_reverse, G);
	ResidualCapacityMap rescapacitymap = get(edge_residual_capacity, G);
	WeightMap weightmap = get(edge_weight, G);
	EdgeAdder eaG(G, capacitymap, revedgemap,weightmap);
	

	//map<pair<int,int>,pair<Edge,pair<int,int>>> edgeMap;

	for(int i=0;i<m;i++) {
		int u= data[i][0]; int v= data[i][1]; int l= data[i][3];
		
		Vertex sV = 2*n+i;
		Vertex sV2 = 2*n+m+i;
		
		//eaG.addEdge(u,v,w);
		eaG.addSingleEdge(u,sV,l);
		eaG.addSingleEdge(sV,v,0);
		eaG.addSingleEdge(v,sV2,l);
		eaG.addSingleEdge(sV2,u,0);
	}
	// Dijkstra shortest paths
	// =======================
	vector<Vertex> predmapS(2*n+2*m);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	vector<long> distmapS(2*n+2*m);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
	dijkstra_shortest_paths(G, s, 
		predecessor_map(make_iterator_property_map(predmapS.begin(), get(vertex_index, G))).	// predecessor map as Named Parameter
		distance_map(make_iterator_property_map(distmapS.begin(), get(vertex_index, G))));	// distance map as Named Parameter
	long distance = distmapS[f];
	if(distance == LONG_MAX) {cout << "0\n"; return;}
	// Dijkstra shortest paths
	// =======================
	vector<Vertex> predmapF(2*n+2*m);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	vector<long> distmapF(2*n+2*m);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
	dijkstra_shortest_paths(G, f, // We MUST provide at least one of the two maps
		predecessor_map(make_iterator_property_map(predmapF.begin(), get(vertex_index, G))).	// predecessor map as Named Parameter
		distance_map(make_iterator_property_map(distmapF.begin(), get(vertex_index, G))));	// distance map as Named Parameter
	
	for(int i=0;i<m;i++) {
		int u= data[i][0]; int v= data[i][1]; int w= data[i][2]; int l= data[i][3];
		if(distmapS[2*n+m+i] + distmapF[2*n+i] == distance+l || distmapS[2*n+i] + distmapF[2*n+m+i] == distance+l) {
			eaG.addEdge(n+u,n+v,w);
			eaG.addEdge(n+v,n+u,w);
		}
	}
	// Calculate flow
	// If not called otherwise, the flow algorithm uses the interior properties
	// - edge_capacity, edge_reverse (read access),
	// - edge_residual_capacity (read and write access).
	long flow1 = push_relabel_max_flow(G, n+s, n+f);
	cout << flow1 << "\n";
}




// Main function to loop over the testcases
int main() {
	ios_base::sync_with_stdio(false);
	int T;	cin>>T;
	for (; T > 0; --T)	testcases();
	return 0;
}
