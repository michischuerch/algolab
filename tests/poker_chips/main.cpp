#include <iostream>
#include <vector>
#include <cmath>

//pokerchips

using namespace std;

long positionFromPointers(vector<int> &m, vector<int> &pointers) {	
	long position = 0;
	int n = pointers.size();
	for(int ii=0;ii<n;ii++) {
		long current = pointers[ii];
		for(int j=0;j<ii;j++) {
			current *= (m[j]+1);
		}
		position += current;
	}
	return position;
}

long recursion(int n, vector<int> &m, vector<int> &pointers, vector<vector<int>> &stacks, vector<int> &memo) {

	vector<bool> done(n,false);
	for(int i=0;i<n;i++) if(pointers[i] >= stacks[i].size()) done[i] = true;
	long solution = 0;
	for(int i=0;i<n;i++) {
		if(pointers[i] < stacks[i].size() && !done[i]) {
			int color = stacks[i][pointers[i]];
			vector<int> equals;
			for(int j=0;j<n;j++) {
				if(!done[j] && pointers[j] < stacks[j].size() && color == stacks[j][pointers[j]]) {
					done[j] = true;
					equals.push_back(j);
				}
			}
			for(int s=1; s < 1 << equals.size(); ++s ){
				vector<int> newPointers = pointers;
				int k=0;
				for ( int ii = 0; ii < equals.size() ; ++ii ){
					if(s & 1 << ii) {
						newPointers[equals[ii]]++;
						k++;
					}
				}
				long points;
				k > 1 ? points=pow(2,k-2) : points=0;
				long position = positionFromPointers(m,newPointers);
				if(memo[position] == -1) solution = max(solution,points+recursion(n,m,newPointers,stacks,memo));
				else solution = max(solution,points+memo[position]);
			}
		}
	}
	memo[positionFromPointers(m,pointers)] = solution;
	return solution;
}

int main() {
	
	int t; cin >> t;
	while(t--) {
		// data
		int n; cin >> n;
		vector<vector<int> > stacks;
		vector<int> m(n);
		for(int i=0;i<n;i++) cin >> m[i];
		for(int i=0;i<n;i++) {
			vector<int> stack(m[i]);
			for(int j=0;j<m[i];j++) cin >> stack[m[i]-j-1]; 
			stacks.push_back(stack);
		}
		
		// algo
		vector<int> pointers(n,0);
		long bigNumba = 1;
		for(int i=0;i<n;i++) bigNumba*=(m[i]+1);
		vector<int> memo(bigNumba,-1);
		
		long result = recursion(n, m, pointers, stacks,memo);
		cout << result << "\n";
		
	}
	
	return 0;
}
