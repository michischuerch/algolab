#include <iostream>
#include <vector>

using namespace std;


long recursion(vector<int> &pointers, vector<vector<int>> &stacks) {
	int n = pointers.size();
	vector<bool> done(n,false);
	vector<long> solutions;
	for(int i=0;i<n;i++) {
		if(!done[i]) {
			int color = stacks[i][pointers[i]];
			vector<int> newPointers(n);
			int counter = 0;
			for(int j=0;j<n;j++) {
				newPointers.push_back(pointers[j]);
				if(color == stacks[j][pointers[j]]) {
					done[j] = true;
					newPointers[j]++;
					counter++;
				}	
			}
			solutions.push_back(recursion(newPointers,stacks));
		}
	}
	
	long maxSolution = -1;
	for(int i=0;i<solutions.size();i++) maxSolution = max(maxSolution,solutions[i]);
	
	return maxSolution;
}

int main() {
	
	int t; cin >> t;
	while(t--) {
		// data
		int n; cin >> n;
		vector<vector<int> > stacks;
		for(int i=0;i<n;i++) {
			int m; cin >> m;
			vector<int> stack(m);
			for(int j=0;j<m;j++) cin >> stack[j]; 
			stacks.push_back(stack);
		}
		
		// algo
		vector<int> pointers(n,0);
		long result = recursion(pointers, stacks);
		
		cout << result << "\n";
	}
	
	return 0;
}
