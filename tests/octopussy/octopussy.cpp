#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

int deactivate_subtree(int i, vector<bool> &deactivated, int n) {
	if(deactivated[i] || i >= n) return 0;
	else {
		deactivated[i] = true;
		return 1 + deactivate_subtree(i*2+1, deactivated, n) + deactivate_subtree(i*2+2, deactivated, n);
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	int t,n;
	cin >> t;
	while(t--) {
		// get data
		cin >> n;
		vector<int> data(n);
		vector<pair<int,int> > sorted;
		vector<bool> deactivated(n,false);
		for(int i=0;i<n;i++) cin >> data[i];
		for(int i=0;i<n;i++) sorted.push_back(make_pair(data[i],i));	
		sort(sorted.begin(),sorted.end());
		bool exploded = false;
		int time = 0;
		int p = 0;
		// algo | idea: [1.] get minimum in tree [2.] deactivate subtree [3.] increase counter / explode
		while(!exploded && p != n) {
			// get minimum
			pair<int,int> min = sorted.at(p);
			// recursively deactivate subtree
			time += deactivate_subtree(min.second,deactivated,n);
			// look if counter is smaller than #nodes in subtree
			if(min.first < time) exploded = true;
			while(p < n && deactivated[sorted[p].second]) p++;
		}
		if(exploded) cout << "no" << "\n";
		else cout << "yes" << "\n";
	}
	return 0;
}
