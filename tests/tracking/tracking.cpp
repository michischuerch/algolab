// ALGOLAB BGL Tutorial 1
// Code snippets demonstrating 
// - graph definitions
// - several algorithms (components, distance-based algorithms, maximum matching)
// - how to pass exterior property maps
// - use of iterators

// Compile and run with one of the following:
// g++ -std=c++11 -O2 bgl-code_snippets.cpp -o bgl-code_snippets; ./bgl-code_snippets
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 bgl-code_snippets.cpp -o bgl-code_snippets; ./bgl-code_snippets

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>
#include <utility>
#include <queue>
#include <limits>
#include <map>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
// Namespaces
using namespace std;
using namespace boost;


// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef adjacency_list<vecS, vecS, directedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		no_property,				// interior properties of vertices	
		property<edge_weight_t, long> 		// interior properties of edges
		>					Graph;
typedef graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef property_map<Graph, edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t

// Functions
// ========= 
void testcases() {
	int V,E,k,x,y;
	cin >> V >> E >> k >> x >> y;
	vector<vector<long> > data;
	int numRivers = 0;
	for (int i = 0; i < E; ++i) {
		long a,b,c,d;
		cin >> a >> b >> c >> d;
		vector<long> d1 = {a,b,c,d};
		data.push_back(d1);
		if(d) numRivers++;
	}
	
	Graph G(V+numRivers*2);
	Graph G2(V+numRivers*2);
	WeightMap weightmap = get(edge_weight, G);
	WeightMap weightmap2 = get(edge_weight, G2);
	int r=0;
	for (int i = 0; i < E; ++i) {
		Vertex u = data[i][0];
		Vertex v = data[i][1];
		long c = data[i][2];
		long d = data[i][3];
		if(!d) {
			Edge e,e2,e3,e4;
			tie(e, ignore) = add_edge(u, v, G);	tie(e3, ignore) = add_edge(u, v, G2);
			tie(e2, ignore) = add_edge(v, u, G); tie(e4, ignore) = add_edge(v, u, G2);
								// Caveat: if u or v don't exist in G, G is automatically extended!
			weightmap[e] = c; weightmap2[e3] = c;
			weightmap[e2] = c; weightmap2[e4] = c;
		} else {
			Edge e,e2,e3,e4,ee1,ee2,ee3,ee4;
			Vertex w = V+r;
			Vertex w2 = V+numRivers+r;
			Vertex w3 = V+2*numRivers+r;
			Vertex w4 = V+3*numRivers+r;
			
			tie(e, ignore) = add_edge(u, w, G); 
			tie(e2, ignore) = add_edge(w, v, G); 
			tie(e3, ignore) = add_edge(v, w2, G); 
			tie(e4, ignore) = add_edge(w2, u, G); 
			tie(ee1, ignore) = add_edge(v, w, G2);
			tie(ee2, ignore) = add_edge(w, u, G2);
			tie(ee3, ignore) = add_edge(u, w2, G2);
			tie(ee4, ignore) = add_edge(w2, v, G2);
			weightmap[e] = c; 
			weightmap[e2] = 0; 
			weightmap[e3] = c; 
			weightmap[e4] = 0; 
			weightmap2[ee1] = 0;
			weightmap2[ee2] = c;
			weightmap2[ee3] = 0;
			weightmap2[ee4] = c;
			r++;
		}
	}
	
	priority_queue<pair<long,pair<int,int> >, vector<pair<long,pair<int,int> > >, greater< pair<long,pair<int,int> > > > riverPaths;
	vector<long> bestDistances();
	
	vector<Vertex> predmap(V+numRivers*2);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	vector<long> distmap(V+numRivers*2);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
	Vertex start = x;
	dijkstra_shortest_paths(G, start, // We MUST provide at least one of the two maps
		predecessor_map(make_iterator_property_map(predmap.begin(), get(vertex_index, G))).	// predecessor map as Named Parameter
		distance_map(make_iterator_property_map(distmap.begin(), get(vertex_index, G))));	// distance map as Named Parameter
	
	long maxl = numeric_limits<long>::max();
	
	// shortest paths from y
	vector<Vertex> predmapY(V+numRivers*2);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	vector<long> distmapY(V+numRivers*2);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
	start = y;
	dijkstra_shortest_paths(G2, start, // We MUST provide at least one of the two maps
	predecessor_map(make_iterator_property_map(predmapY.begin(), get(vertex_index, G2))).	// predecessor map as Named Parameter
	distance_map(make_iterator_property_map(distmapY.begin(), get(vertex_index, G2))));	// distance map as Named Parameter
	
	for(int i=0;i<numRivers*2;i++) {
		if(distmap[V+i] != maxl && distmapY[V+i] != maxl) {
			riverPaths.push(make_pair(distmap[V+i]+distmapY[V+i],make_pair(i,1)));
		}
	}
	long bestPath = maxl;
	vector<int,vector<long> > shortestPaths;
	int numDij = 0;
	
	while(!riverPaths.empty()) {
		
		pair<long,pair<int,int> > riverNode = riverPaths.top();
		riverPaths.pop();
		if(riverNode.second.second >= k && riverNode.first >= bestPath) break;
		
		if(riverNode.second.second >= k) bestPath = min(bestPath,riverNode.first);
		else {
			Vertex start = V + riverNode.second.first;
			if(shortestPaths.count(start) == 0) {
				numDij++;
				dijkstra_shortest_paths(G, start, // We MUST provide at least one of the two maps
					predecessor_map(make_iterator_property_map(predmap.begin(), get(vertex_index, G))).	// predecessor map as Named Parameter
					distance_map(make_iterator_property_map(distmap.begin(), get(vertex_index, G))));	// distance map as Named Parameter
				shortestPaths[start] = distmap;
			}
			else distmap = shortestPaths[start];
			for(int i=0;i<numRivers*2;i++) {
				if(distmap[i+V] == maxl || distmapY[i+V] == maxl) continue;
				if(i != riverNode.second.first) riverPaths.push(make_pair(riverNode.first+distmap[i+V]-distmapY[V+riverNode.second.first]+distmapY[V+i],make_pair(i,riverNode.second.second+1)));
			}
		}
	}
	//cout << numDij << " ";
	
	
	cout << bestPath << "\n";
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin >> T;
	while(T--)	testcases();
	return 0;
}

