// sith

#include <iostream>
#include <queue>
#include <algorithm>
#include <climits>

using namespace std;

long squared_distance(vector<pair<int,int>> &data, int i,int j) {
	long dx = data[i].first-data[j].first;
	long dy = data[i].second-data[j].second;
	return dx*dx+dy*dy;
}

int testcases()
{
	int n; cin>>n;
	long r; cin>>r;
	long squaredR = r*r;
	//K::Point_2 P[n];
	vector<pair<int,int>> data(n);

	vector<vector<int>> ns(n); // neighbours of each point
	for (int i = 0; i < n; ++i) {
		int x,y; cin>>x>>y;
		data[i] = make_pair(x,y);
		for(int j=0;j<i;j++) {
			if(squared_distance(data,i,j) <= squaredR) {
				ns[i].push_back(j);
				ns[j].push_back(i);
			}
		}
	}
	int best=1;
	for(int i=n-1;i>=best;) {
		vector<bool> vis(n,false);
		priority_queue<int> Q;
		Q.push(i);
		vis[i]=true;
		int counter=0;
		int minElement = INT_MAX;
		while (!Q.empty()) {
			const int u = Q.top();
			Q.pop();
			minElement=min(minElement,u);
			counter++;
			if(counter==minElement) break;
			else if(counter > minElement) {
				counter--; 
				break;
			}
			
			for(int j=0;j<ns[u].size();j++) {
				if(vis[ns[u][j]]) continue;
				vis[ns[u][j]]=true;
				Q.push(ns[u][j]);
			}
		}
		/*while (!Q.empty()) {
			const int u = Q.top();
			Q.pop();
			vis[u] = false;
		}*/
		best = max(best,counter);
		
		while(i >= best && vis[i]) i--;
	}
	cout << best <<"\n";
	return 0;
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
