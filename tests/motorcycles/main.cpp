#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <vector>
#include <utility>

// motorcycles
typedef CGAL::Exact_predicates_exact_constructions_kernel K;

using namespace std;


// idea: 
// 1. calculate all angles
// 2. sort wrt y values
// 3. go top down -> take only decereasing angles >= 0
// 4. go bottom up take only increasing angles <= 0 (care for special case where we have an angle > 0 before and now an angle < 0 and they are equal)

int main() {
	ios_base::sync_with_stdio(false);
	int t; cin >> t;
	while(t--) {
		// read in data
		int n; cin >> n;
		vector<pair<long,pair<K::FT,int>>> data;
		for(int i=0;i<n;i++) {
			long y0,x1,y1;
			cin >> y0 >> x1 >> y1;
			K::FT val = (K::FT(y1)-K::FT(y0))/K::FT(x1);
			data.push_back(make_pair(y0,make_pair(val,i)));
		}
		// sort data
		sort(data.begin(),data.end());
		vector<bool> conflicts(n,true);
		
		K::FT angle = data[n-1].second.first;
		for(int i=n-1;i>=0;i--) 
			if(abs(data[i].second.first) <= abs(angle)) {
				angle = data[i].second.first;
				if(angle >= 0) conflicts[data[i].second.second] = false;
			}

		angle = data[0].second.first;
		for(int i=0;i<n;i++) {
			if(!conflicts[data[i].second.second]) break;
			if(angle > 0 && data[i].second.first < 0 && abs(data[i].second.first) == abs(angle));
			else if(abs(data[i].second.first) <= abs(angle)) {
				angle = data[i].second.first;
				if(angle <= 0) conflicts[data[i].second.second] = false;
			}
		}
		for(int i=0;i<n;i++) if(!conflicts[i]) cout << i << " ";
		cout << "\n";
	}
	return 0;
}
