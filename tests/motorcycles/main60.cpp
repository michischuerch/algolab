#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <vector>
#include <utility>

// motorcycles
typedef CGAL::Exact_predicates_exact_constructions_kernel K;

using namespace std;

int main() {
	ios_base::sync_with_stdio(false);
	int t; cin >> t;
	while(t--) {
		// read in data
		int n; cin >> n;
		vector<pair<long,pair<K::FT,int>>> data;
		for(int i=0;i<n;i++) {
			long y0,x1,y1;
			cin >> y0 >> x1 >> y1;
			K::FT val = (K::FT(y1)-K::FT(y0))/K::FT(x1);
			data.push_back(make_pair(y0,make_pair(val,i)));
		}
		// sort data
		sort(data.begin(),data.end());
		vector<bool> conflicts(n,false);
		K::FT angle = data[n-1].second.first;
		for(int i=n-2;i>=0;i--) {
			if(data[i].second.first <= angle) angle = data[i].second.first;
			else conflicts[data[i].second.second] = true;
		}
		for(int i=0;i<n;i++) if(!conflicts[i]) cout << i << " ";
		cout << "\n";
	}
	return 0;
}
