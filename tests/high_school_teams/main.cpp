// high_school_teams

// idea: use split and list for the students, sort and then aggregate the second subset sums
// also used: memoization over k

#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

void subsetsum (vector<int> data, int k, vector<int> &result) {
	int n=data.size();
	vector<int> masks;
	for ( int s = 0; s < 1 << n ; ++s ){ // iterate through all subsets
		int sum = 0;
		for ( int i = 0; i < n ; ++i ){
			if( s & 1 << i ) sum++; // if i-th element in subset
		}
		if(sum==k)masks.push_back(s);
	}
	for(int mask : masks) {
		
		int n_ = n-k;
		vector<int> data_;
		for(int i=0;i<n;i++) if(!(mask & 1 << i)) data_.push_back(data[i]);
		
		for ( int s = 0; s < 1 << n_ ; ++s ){ // iterate through all subsets
			int sum1 = 0;
			int sum2 = 0;
			for ( int i = 0; i < n_ ; ++i ){
				if( s & 1 << i ) sum1 += data_[i]; // if i-th element in subset
				else sum2 += data_[i]; // if i-th element in subset
			}
			result.push_back(sum1-sum2);
		}
	}
}

void testcases() {
	int n,k; cin>>n>>k;
	vector<int> data(n);
	for(int i=0;i<n;i++) cin>>data[i];
	
	vector<int> data1, data2;
	for(int i=0;i<n;i++) i<n/2 ? data1.push_back(data[i]) : data2.push_back(data[i]);
	
	long result = 0;
	vector<vector<int>> memoSubset1(k+1);
	vector<vector<pair<int,int>>> memoSubset2(k+1);
	
	for(int kk=0;kk<=k;kk++) {
		for(int kkk=0;kkk<=kk;kkk++) {
			vector<int> subsetSum1, subsetSum2;
			vector<pair<int,int>> subset2List;
			if(memoSubset1[kkk].empty()) {
				subsetsum (data1, kkk, subsetSum1);
				memoSubset1[kkk] = subsetSum1;
			}
			else subsetSum1 = memoSubset1[kkk];
			if(memoSubset2[kk-kkk].empty()) {
				subsetsum (data2, kk-kkk, subsetSum2);
				
				sort(subsetSum2.begin(),subsetSum2.end());
			
				for(int i=0;i<subsetSum2.size();) {
					int amount = 0;
					int now = subsetSum2[i];
					while(i<subsetSum2.size() && now == subsetSum2[i]) {
						amount++;
						i++;
					}
					subset2List.push_back(make_pair(now,amount));
				}
				memoSubset2[kk-kkk] = subset2List;
			}
			else subset2List = memoSubset2[kk-kkk];
			
			for(int sum1 : subsetSum1) {
				auto lower = lower_bound(subset2List.begin(), subset2List.end(), make_pair(-sum1,0));
				if(lower!=subset2List.end() && lower->first == -sum1) result+=lower->second;
			}
		}
	}
	cout<<result<<"\n";
}
// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false);
	int T;	cin >> T;	// First input line: Number of testcases.
	while(T--)	testcases();
	return 0;
}

