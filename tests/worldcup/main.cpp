// world cup

// example: decide whether there exists a disk that covers a given set
// of points R in the plane and is disjoint from another set of points B
#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

// choose exact integral type
typedef CGAL::Gmpq ET;

// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;
typedef CGAL::Quotient<ET> SolT;


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;


using namespace std;

// round up to next integer double
double ceil_to_double(const SolT& x)
{
  double a = std::ceil(CGAL::to_double(x));
  while (a < x) a += 1;
  while (a-1 >= x) a -= 1;
  return a;
}

int testcases() {
	
	Program lp (CGAL::SMALLER, true, 0, false, 0);
	// read points
	std::vector<K::Point_2> pts;
	
	int n,m,c; cin>>n>>m>>c;
	
	pts.reserve(n+m);
	
	
	vector<vector<long>> ws;
	for(int i=0;i<n;i++) {
		long x,y,s,a; cin>>x>>y>>s>>a;
		vector<long> wi = {x,y,s,a};
		ws.push_back(wi);
		
		K::Point_2 p(x,y);
		pts.push_back(p);
		
	}
	vector<vector<long>> ss;
	for(int j=0;j<m;j++) {
		long x,y,d,u;cin>>x>>y>>d>>u;
		vector<long> si = {x,y,d,u};
		ss.push_back(si);
		
		K::Point_2 p(x,y);
		pts.push_back(p);
	}
	vector<vector<long>> rs;
	for(int i=0;i<n;i++) {
		vector<long> ri;
		for(int j=0;j<m;j++) {
			long rij; cin>> rij;
			ri.push_back(rij*100);
			
		}
		rs.push_back(ri);
	}
	
	// construct triangulation
	Triangulation t;
	t.insert(pts.begin(), pts.end());
	
	int numPoints = 0;
	vector<vector<long>> cs;
	vector<K::Point_2> cs_points;
	for(int i=0;i<c;i++) {
		long x,y,r; cin>>x>>y>>r;
		vector<long> ci = {x,y,r*r};
		if(numPoints==100) continue;
		// check if important
		K::Point_2 p(x,y);
		if(CGAL::squared_distance(t.nearest_vertex(p)->point(),p) <= r*r ) {
			numPoints++;
			cs.push_back(ci);
			
			cs_points.push_back(K::Point_2(x,y));
		}
	}
	
	vector<vector<int>> crossings(n+m);
	for(int i=0;i<n+m;i++) {
		vector<int> cross;
		for(int cc=0;cc<cs.size();cc++) {
			if(CGAL::squared_distance(pts[i],cs_points[cc]) <= cs[cc][2])
				cross.push_back(cc);
		}
		crossings[i]=cross;
	}
	
	for(int i=0;i<n;i++) {
		for(int j=0;j<m;j++) {
			int li=0;
			int lj=0;
			
			vector<int> ci = crossings[i];
			vector<int> cj = crossings[n+j];
			
			int num = 0;
			while(li < ci.size() && lj < cj.size()) {
				if(ci[li] == cj[lj]) {
					li++;
					lj++;
				} else if(ci[li] < cj[lj]) {
					num++;
					li++;
				} else {
					num++;
					lj++;
				}
			}
			while(li < ci.size()) {
				li++;
				num++;
			} 
			while(lj < cj.size()) {
				lj++;
				num++;
			}
			//cout << num << " ";
			//if(num!= 0) cout << num << " ";
			rs[i][j] -= num;
			//cout << ET(rs[i][j])/100 << " ";
		}
	}
	
	
	for(int i=0;i<n;i++) {
		for(int j=0;j<m;j++) {
			
			lp.set_a (i*m+j, i, 1);
			lp.set_b (       i, ws[i][2]);
			lp.set_a (i*m+j, n+j, ws[i][3]);
			lp.set_b (       n+j, ss[j][3]*100);
			lp.set_a (i*m+j, n+m+j, 1);
			lp.set_b (       n+m+j, ss[j][2]);
			lp.set_r(    n+m+j, CGAL::EQUAL);
			lp.set_c(i*m+j, ET(-rs[i][j])/100);
			
		}
	}
	
	
	
	// solve the program, using ET as the exact type
	Solution s = CGAL::solve_linear_program(lp, ET());
	assert (s.solves_linear_program(lp));
	
	// output exposure center and radius, if they exist
	if (s.is_optimal()) { // && (s.objective_value() < 0)
		// *opt := alpha, *(opt+1) := beta, *(opt+2) := gamma
		CGAL::Quadratic_program_solution<ET>::Variable_value_iterator
		  opt = s.variable_values_begin();
		
		/*bool check = true;
		for(int j = 0;j<m;j++) {
			CGAL::Quotient<ET> amount(0);
			for(int i=0;i<n;i++) {
				amount += *(opt+i*m+j);
			}
			
			if(amount != ET(ss[j][2])) {
				check=false;
				break;
			}
			//cout << amount << " " << ss[j][2];
		}*/
		//cout << s;
		//double a = std::floor(CGAL::to_double(s.objective_value()));
		//while (a > s.objective_value()) a -= 1;
		//while (a+1 <= s.objective_value()) a += 1;
		//if(a!=0) a=-a;
		SolT solution = s.objective_value();
		//cout << solution << " ";
		if(solution==0) cout << "0";
		else cout << (long)-ceil_to_double(solution);
		//else cout << -(long)ceil_to_double(solution);
		//cout << "works";
		/*CGAL::Quotient<ET> alpha = *opt;
		CGAL::Quotient<ET> beta = *(opt+1);
		CGAL::Quotient<ET> gamma = *(opt+2);
		std::cout << "There is a valid exposure:\n";
		std::cout << " Center = ("        // (alpha/2, beta/2)
			  << alpha/2 << ", " <<  beta/2
			  << ")\n";
		std::cout << " Squared Radius = " // gamma + alpha^2/4 + beta^2/4
			  << gamma + alpha*alpha/4 + beta*beta/4 << "\n";*/
	} else cout << "RIOT!";
	cout << "\n";
	
	
	
	/*
  // by default, we have an LP with Ax <= b and no bounds for
  // the four variables alpha, beta, gamma, delta
  Program lp (CGAL::SMALLER, false, 0, false, 0);
  const int alpha = 0;
  const int beta  = 1;
  const int gamma = 2;
  const int delta = 3;

  // number of red and blue points
  int m; std::cin >> m;
  int n; std::cin >> n;

  // read the red points (cancer cells)
  for (int i=0; i<m; ++i) {
    int x; std::cin >> x;
    int y; std::cin >> y;
    // set up <= constraint for point inside/on circle:
    //   -alpha x - beta y - gamma <= -x^2 - y^2
    lp.set_a (alpha, i, -x);
    lp.set_a (beta,  i, -y);
    lp.set_a (gamma, i, -1);
    lp.set_b (       i, -x*x - y*y);
  }
  // read the blue points (healthy cells)
  for (int j=0; j<n; ++j) {
    int x; std::cin >> x;
    int y; std::cin >> y;
    // set up <= constraint for point outside circle:
    //   alpha x + beta y + gamma + delta <= x^2 + y^2
    lp.set_a (alpha, m+j, x);
    lp.set_a (beta,  m+j, y);
    lp.set_a (gamma, m+j, 1);
    lp.set_a (delta, m+j, 1);
    lp.set_b (       m+j, x*x + y*y);
  }

  // objective function: -delta (the solver minimizes)
  lp.set_c(delta, -1);

  // enforce a bounded problem:
  lp.set_u (delta, true, 1);

  // solve the program, using ET as the exact type
  Solution s = CGAL::solve_linear_program(lp, ET());
  assert (s.solves_linear_program(lp));

  // output exposure center and radius, if they exist
  if (s.is_optimal() && (s.objective_value() < 0)) {
    // *opt := alpha, *(opt+1) := beta, *(opt+2) := gamma
    CGAL::Quadratic_program_solution<ET>::Variable_value_iterator
      opt = s.variable_values_begin();
    CGAL::Quotient<ET> alpha = *opt;
    CGAL::Quotient<ET> beta = *(opt+1);
    CGAL::Quotient<ET> gamma = *(opt+2);
    std::cout << "There is a valid exposure:\n";
    std::cout << " Center = ("        // (alpha/2, beta/2)
	      << alpha/2 << ", " <<  beta/2
	      << ")\n";
    std::cout << " Squared Radius = " // gamma + alpha^2/4 + beta^2/4
	      << gamma + alpha*alpha/4 + beta*beta/4 << "\n";
  } else
    std::cout << "There is no valid exposure.";
  std::cout << "\n";
  return 0;
  * */
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}

