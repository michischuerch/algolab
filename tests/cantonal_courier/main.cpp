// cantonal courier

// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>

// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>


// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;
typedef	boost::graph_traits<Graph>::out_edge_iterator			OutEdgeIt;


// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
	}
};


using namespace std;

int testcases() {
	int Z,J; cin>>Z>>J;
	
	int sss = Z+J;
	int ttt = Z+J+1;
	// Create Graph and Maps
	Graph G(Z+J+2);
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);
	
	vector<int> cs(Z); 
	int allC=0;
	for(int i=0;i<Z;i++) {
		cin>>cs[i];
		allC+=cs[i];
	}
	vector<int> ps(J);
	int allP=0;
	for(int i=0;i<J;i++) {
		cin>>ps[i];
		allP += ps[i];
	}
	
	for(int i=0;i<Z;i++) {
		eaG.addEdge(sss, i, cs[i]);
	}
	for(int j=0;j<J;j++) {
		eaG.addEdge(Z+j, ttt, ps[j]);
	}
	
	
	for(int i=0;i<J;i++) {
		int Ni; cin>>Ni;
		
		for(int j=0;j<Ni;j++) {
			int z_i; cin>>z_i;
			eaG.addEdge(z_i, Z+i, INT_MAX);
		}
	}
	int benefit = 0;
	//cout << benefit << " ";
	long flow1 = boost::push_relabel_max_flow(G, sss, ttt);
	
	// BFS to find vertex set S
	std::vector<int> vis(Z+J+2, false); // visited flags
	std::queue<int> Q; // BFS queue (from std:: not boost::)
	vis[sss] = true; // Mark the source as visited
	Q.push(sss);
	while (!Q.empty()) {
		const int u = Q.front();
		Q.pop();
		OutEdgeIt ebeg, eend;
		for (boost::tie(ebeg, eend) = boost::out_edges(u, G); ebeg != eend; ++ebeg) {
			const int v = boost::target(*ebeg, G);
			// Only follow edges with spare capacity
			if (rescapacitymap[*ebeg] == 0 || vis[v]) continue;
			vis[v] = true;
			Q.push(v);
		}
	}
	for(int i=0;i<Z+J;i++) 
		if(i<Z) {
			if(!vis[i]) {
				benefit -= cs[i];
				//cout << cs[i] << " ";
			}
		} else {
			if(!vis[i]) {
				benefit += ps[i-Z];
			}
		}

	/*vector<int> benefit(Z+1,0);
	for(int i=0;i<Z;i++) benefit[i] -= cs[i];
	//vector<vector<bool>> cl; // J x Z
	for(int i=0;i<J;i++) {
		int Ni; cin>>Ni;
		if(Ni == 0) benefit[Z] += ps[i];
		
		for(int j=0;j<Ni;j++) {
			int z_i; cin>>z_i;
			benefit[z_i] += ps[i];
		}
	}*/
	//eaG.addEdge(sss,ttt,1);
	/*for(int i=0;i<Z;i++) {
		eaG.addEdge(sss, i, loss[i]); // from, to, capacity
		eaG.addEdge(i, ttt, INT_MAX); // from, to, capacity
	}*/
	
	/*long best = 0;
	for(int i=0;i<Z+1;i++) {
		if(benefit[i] > 0) best += benefit[i];
	}*/
	
	
	//long flow1 = 0;
	//long flow1 = boost::push_relabel_max_flow(G, sss, ttt);
	/*
	int current = allC-allP;
	int best = current;
	bool running = true;
	while(running) {
		int z = getNextZoneToRemove(cs, ps, cl);
		if(z==-1) {
			running = false;
			break;
		}
		current -= removeJobs(z, cs, ps, cl);
		
		//cout << " " << current << ")";
		best = max(best,current);
		
	}*/
	cout << benefit;
	cout << "\n";
	
	
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
