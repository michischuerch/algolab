// cantonal courier

// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>

using namespace std;

// -1 if no good zone
int getNextZoneToRemove(vector<int> &cs, vector<int> &ps, vector<vector<bool>> &cl) {
	int Z=cs.size();
	int J=ps.size();
	
	int bestZone = -1;
	int bestImprovement = INT_MIN;
	
	for(int i=0;i<Z;i++) {
		int currentImprovement=cs[i];
		
		for(int j=0;j<J;j++) {
			if(cl[j][i]) currentImprovement-= ps[j];
		}
		
		if(bestImprovement < currentImprovement) {
			bestImprovement = currentImprovement;
			bestZone = i;
		}
	}
	
	//~ cout << "(" << bestZone << " "<<bestImprovement;// ") ";
	
	return bestZone;
	
}

int removeJobs(int z, vector<int> &cs, vector<int> &ps, vector<vector<bool>> &cl) {
	int Z=cs.size();
	int J=ps.size();
	
	int gain = 0;
	
	gain+=cs[z];
	cs.erase(cs.begin()+z);
	vector<int> newPs;
	for(int j=0;j<J;j++) {
		if(cl[j][z]) gain-=ps[j];
		else if(!cl[j][z]) newPs.push_back(ps[j]);
		cl[j].erase(cl[j].begin()+z);
	}
	ps = newPs;	
	
	return gain;
}

int testcases() {
	int Z,J; cin>>Z>>J;
	
	vector<int> cs(Z); 
	int allC=0;
	for(int i=0;i<Z;i++) {
		cin>>cs[i];
		allC+=cs[i];
	}
	vector<int> ps(J);
	int allP=0;
	for(int i=0;i<J;i++) {
		cin>>ps[i];
		allP += ps[i];
	}
	
	vector<vector<bool>> cl; // J x Z
	for(int i=0;i<J;i++) {
		int Ni; cin>>Ni;
		vector<bool> l_i(Z,0);
		for(int j=0;j<Ni;j++) {
			int z_i; cin>>z_i;
			l_i[z_i] = 1;
		}
		cl.push_back(l_i);
	}
	
	int current = allC-allP;
	int best = current;
	bool running = true;
	while(running) {
		int z = getNextZoneToRemove(cs, ps, cl);
		if(z==-1) {
			running = false;
			break;
		}
		current -= removeJobs(z, cs, ps, cl);
		
		//cout << " " << current << ")";
		best = max(best,current);
		
	}
	cout << -best << " ";
	cout << "\n";
	
	
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
