#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

bool hasConflict(pair<int,int> a, pair<int,int> b) {
	int a1 = a.first;
	int a2 = a.second;
	int b1 = b.first;
	int b2 = b.second;
		
	if (a1 <= a2) {
		if ((b1 >= a1 && b1 <= a2) || (b2 >= a1 && b2 <= a2)) return true;
		else return false;
	} else {
		if ((b1 <= a1 && b1 >= a2) || (b2 <= a1 && b2 >= a2)) return true;
		else return false;
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	bool debug = false;
	bool debug2 = true;
	int t;
	cin >> t;
	while(t--) {
		// data
		int n, m;
		cin >> n >> m;
		
		vector<pair<int,int> > intervals;
		vector<pair<int,int> > blength;
		for(int i=0;i<n;i++) {
			int a,b;
			cin >> a >> b;
			intervals.push_back(make_pair(a,b));
			if(a <= b) blength.push_back(make_pair(b-a,i));
			else blength.push_back(make_pair(a+m-b,i));
		}
		sort(intervals.begin(),intervals.end());
		sort(blength.begin(),blength.end());
		//sort(be_p.begin(),be_p.end());
		
		// look for interval with least amount of conflicts
		/*vector<bool> boatIsOpen(n);
		int openBoats = 0;
		for(int i=0;i<be_p.size();i++) {
			tuple<int,int,int> next_be = be_p[i];
			if(get<2>(next_be) == 0) {
				boatIsOpen[intervals[get<1>(next_be)] = true;
				openBoats++;
			} else {
				if(boatIsOpen[intervals[get<1>(next_be)] == true)
			}
		}*/
		
		/*for(int i=0;i<intervals.size();i++) {
			if()
		}*/
		
		vector<int> interestingBoats;
		interestingBoats.push_back(blength[0].second);
		if(blength[0].first != 1) {
			// look for all colliding boats
			pair<int,int> b = intervals[interestingBoats[0]];
			for(int i=0;i<n;i++) 
				if(i!=blength[0].second && hasConflict(b,intervals[i])) interestingBoats.push_back(i);
		}
		
		cout << "bs(" << interestingBoats.size() << ") ";
		//for(int i=0;i<intervals.size();i++) cout << intervals[i].first << " " << intervals[i].second << "| ";
		int bestSolution = 0;
		for(int i=0;i<interestingBoats.size();i++) {
			
			//cout << "start(" << intervals[i].first << "," << intervals[i].second << ")";
			
			vector<pair<int,int> > boats;
			for(int ii=i+1;ii < n;ii++) {
				if(intervals[ii].first > intervals[i].second) {
					if(intervals[ii].first <= intervals[ii].second) boats.push_back(intervals[ii]);
					if(intervals[ii].second < intervals[i].first) boats.push_back(make_pair(intervals[ii].first,intervals[ii].second+m));
				}
			}
			
			/*for(int ii=0;ii<i;ii++) {
				if(intervals[i].first <= intervals[i].second) 
					pair<int,int> boat = make_pair(interval[ii].first,interval[ii].second);
					if(intervals[ii].second < intervals[ii].first) interval.second+=m;
					if(intervals[ii].first > interval[)
					if(intervals[ii].second < intervals[i].first) boats.push_back(make_pair(intervals[ii].first+m,intervals[ii].second+m));
				//else if(intervals[ii].first > intervals[i].second) boats.push_back(make_pair(intervals[ii].first+m,intervals[ii].second+m));
			}*/
			/*cout << "boats: ";
			for(int i=0;i<boats.size();i++) cout << boats[i].first << " " << boats[i].second << " | ";*/
			
			int solution = 1;
			//cout << "m" << currentM << " ";
			// perform boats
			//int endLine = intervals[i].first;
			int line = intervals[i].second;
			
			for(int k=0;k < boats.size();k++) {
				// take next boat
				if(debug) cout << "b(" << boats[k].first << " " << boats[k].second << ") ";
				if(boats[k].first > boats[k].second) cout << "wtf";
				if(boats[k].first > line) {
					// check if there is a better boat
					int kk = k+1;
					int upperBound = boats[k].second;
					while(kk < boats.size() && boats[kk].first < upperBound) {
						if(boats[kk].second < upperBound) {
							upperBound = boats[kk].second;
							k = kk;
						}
						kk++;
					}
					line = upperBound;
					//cout << "boat(" << boats[k].first << " " << boats[k].second << ") ";
					//cout << "ub(" << upperBound << ") ";
					solution++;
				}
			}
			bestSolution = max(solution,bestSolution);
			if(debug) cout << "bs(" << bestSolution << ") ";
			//return 0;
		}
		
		
		cout << bestSolution << "\n";
	}
	return 0;
}
