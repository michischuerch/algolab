#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <map>

using namespace std;

// BOATS 100 points
// Main ideas:
// 1. sort intervals, and begin / end+1
// 2. accumulate begin/end and find the starting point
// 3. find all boats that overlap the starting point
// 4. for all boats that overlap perform the boats algo from last week and also look at the case where we dont choose boat for the spot

int main() {
	ios_base::sync_with_stdio(false);
	int t;
	cin >> t;
	while(t--) {
		// data
		int n, m;
		cin >> n >> m;
		vector<pair<int,int> > intervals;
		vector<pair<int,int> > be;
		for(int i=0;i<n;i++) {
			int a,b;
			cin >> a >> b;
			intervals.push_back(make_pair(a,b));
			be.push_back(make_pair(a,1));
			// note that I end the boats one to the right because this is the spot where the boat count can actually be decreased (boats cannot overlap)
			be.push_back(make_pair(b+1,-1));
		}
		sort(intervals.begin(),intervals.end());
		sort(be.begin(),be.end());
		
		// idea here is to accumulate all starts and ends that are in the same bucket so we dont have to bother when finding the starting point
		vector<pair<int,int> > be_accumulated;
		for(int i=0;i<be.size();i++) {
			pair<int,int> curr = be[i];
			int counter = curr.second;
			for(int j=i+1;j<n;j++) {
				if(curr.first == be[j].first) {
					i = j;
					counter+=be[j].second;
				}
				else break;
			}
			be_accumulated.push_back(make_pair(curr.first,counter));
		}
		// find good starting point
		// idea: find the spot that has only 10 boats overlapping
		int numberOfBoats = 0;
		for(int i=0;i<n;i++) if(intervals[i].first > intervals[i].second) numberOfBoats++;
		int startPoint = -1;
		for(int i=0;i<be_accumulated.size();i++) {
			numberOfBoats+=be_accumulated[i].second;
			if(numberOfBoats <= 10) {
				startPoint = be_accumulated[i].first;
				break;
			}
		}
		// find all boats that overlap the good spot (at most 10)
		vector<int> startPoints;
		for(int i=0;i<n;i++) {
			if(intervals[i].first <= intervals[i].second && intervals[i].first <= startPoint && intervals[i].second >= startPoint) startPoints.push_back(i);
			else if(intervals[i].first > intervals[i].second && (intervals[i].first <= startPoint || intervals[i].second >= startPoint)) startPoints.push_back(i);
		}
		int bestSolution = 0;
		intervals.push_back(make_pair(startPoint,startPoint));
		startPoints.push_back(intervals.size()-1);
		// idea: start with each boat in the list "startPoints", which are our starting intervals and also start with no boat.
		// so at most 11 cases will be looked at.
		// Then shift the remaining boats such that they start with 1 and 
		// perform the Boats algorithm from last week
		for(int i=0;i<startPoints.size();i++) {
			int solution = 0;
			int line = 0;
			int mBound = m;
			
			int boatLeft = intervals[startPoints[i]].first;
			int boatRight = intervals[startPoints[i]].second;
			
			// build our list of boats
			// idea: remove all boats that overlap our chosen boat StartPoints[i] and shift all boats that start to the right of our chosen boat to the left s.t. they start with 1
			// then append all boats that start on the left of our chosen boat
			// note: this was super tedious to think about and implement properly (alot of off by one errors in the beginning...), maybe there is a more easy way to implement this
			vector<pair<int,int> > boats;
			for(int ii=0;ii<n;ii++) {
				if(boatLeft <= boatRight) {
					if(intervals[ii].first > boatRight) {
						if(intervals[ii].first <= intervals[ii].second) {
							boats.push_back(make_pair(intervals[ii].first-boatRight,intervals[ii].second-boatRight));
						} else if(intervals[ii].second < boatLeft){
							boats.push_back(make_pair(intervals[ii].first-boatRight,intervals[ii].second+m-boatRight));
						}
					} else if(intervals[ii].first < boatRight) {
						if(intervals[ii].first <= intervals[ii].second && intervals[ii].second < boatRight) {
							boats.push_back(make_pair(intervals[ii].first+m-boatRight,intervals[ii].second+m-boatRight));
						}
					}
				} else {
					if(intervals[ii].first > boatRight && intervals[ii].second < boatLeft) {
						if(intervals[ii].first <= intervals[ii].second) {
							boats.push_back(make_pair(intervals[ii].first-boatRight,intervals[ii].second-boatRight));
						} 
					} 
				}
			}
			// resort the list because we may have fucked up our order (we need for boats algo)
			sort(boats.begin(),boats.end());
			if(boatLeft <= boatRight) mBound = m-boatRight+boatLeft-1;
			else mBound = boatLeft - boatRight;
			// if we are not in the empty boat case we have already ne boat
			if(i < startPoints.size() - 1) solution++; 
			// BOATS BOATS BOATS BOATS
			for(int k=0;k < boats.size();k++) {
				if(boats[k].first > line && boats[k].second <= mBound) {
					// check if there is a better boat
					int kk = k+1;
					int upperBound = boats[k].second;
					while(kk < boats.size() && boats[kk].first < upperBound) {
						if(boats[kk].second < upperBound) {
							upperBound = boats[kk].second;
							k = kk;
						}
						kk++;
					}
					line = upperBound;
					solution++;
				}
			}
			bestSolution = max(solution,bestSolution);
		}
		cout << bestSolution << "\n";
	}
	return 0;
}
