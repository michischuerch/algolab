#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

bool check_conflict(pair<int,int> a, pair<int,int> b,int m) {
	int a1 = a.first;
	int a2 = a.second;
	int b1 = b.first;
	int b2 = b.second;
	
	if(a1 <= a2 && b1 <= b2) {
		if((a1 <= b2 && a1 >= b1) || (a2 <= b2 && a2 >= b1)) return true;
		else return false;
	} 
	else if(a1 > a2 && b1 > b2) 
		return check_conflict(make_pair(a1,m), make_pair(b1,m),m) ||
		check_conflict(make_pair(1,a2), make_pair(b1,m),m) ||
		check_conflict(make_pair(a1,m), make_pair(1,b2),m) ||
		check_conflict(make_pair(1,a2), make_pair(1,b2),m);
	else if(a1 > a2 && b1 <= b2) 
		return check_conflict(make_pair(a1,m), b,m) ||
		check_conflict(make_pair(1,a2), b,m);
	else if(a1 <= a2 && b1 > b2) 
		return check_conflict(a, make_pair(b1,m),m) ||
		check_conflict(a, make_pair(1,b2),m);
}

int main() {
	ios_base::sync_with_stdio(false);
	int t;
	cin >> t;
	while(t--) {
		int n, m;
		cin >> n >> m;
		
		vector<pair<int,int> > intervals;
		for(int i=0;i<n;i++) {
			int a,b;
			cin >> a >> b;
			intervals.push_back(make_pair(a,b));
		}
		
		vector<vector<int> > conflicts(n);
		for(int i=0;i<n;i++) {
			pair<int,int> current_interval = intervals[i];
			for(int j=0;j<n;j++) {
				if(i!=j && check_conflict(current_interval,intervals[j],m)) {
					conflicts[i].push_back(j);
				}
			}
		}
		
		vector<pair<int,int> > conflictAmounts(n);
		for(int i=0;i<n;i++) conflictAmounts[i] = make_pair(conflicts[i].size(),i);
		sort(conflictAmounts.begin(),conflictAmounts.end());
		
		vector<pair<int,int> > solutionSet;
		solutionSet.push_back(intervals[conflictAmounts[0].second]);
		int number=0;
		for(int i=1;i<n;i++) {
			pair<int,int> nextInterval = intervals[conflictAmounts[i].second];
			bool useful = true;
			for(int j=0;j<solutionSet.size();j++) {
				if(check_conflict(solutionSet[j],nextInterval,m)) {
					useful = false;
					break;
				}
			}
			if(useful) {
				solutionSet.push_back(nextInterval);
				number++;
			}
		}
		
		for(int i=0;i<solutionSet.size();i++) {
			cout << solutionSet[i].first << " " << solutionSet[i].second << "| ";
		}
		
		cout << number << "\n";
	}
	return 0;
}
