#include <iostream>
#include <vector>

using namespace std;

int main() { 
	
	int t,n,k,x;
	cin >> t;
	while(t--){
		cin >> n;
		cin >> k;
		
		vector<int> data(n,0);
		
		for(int i=0; i<n;i++) {
			cin >> x;
			data[i] = x;
		}
		
		int bestLeft = 0;
		int bestRight = 0;
		int bestSum = 0;
		
		int left = 0;
		int right = 0;
		int sum = data[0];
		
		while(left != n - 1) {
			if(abs(sum - k) < abs(bestSum - k)) {
				bestLeft = left;
				bestRight = right;
				bestSum = sum;
			}
			
			if(sum == k) {
				break;
			}
			if(sum < k) {
				if(right == n - 1) {
					break;
				}
				
				right++;
				sum+=data[right];
				
			} else if(sum > k) {
				
				if(left == right) {
					right++;
					sum+=data[right];
					sum-=data[left];
					left++;
				} else {
					sum-=data[left];
					left++; 
				}
			}
			
			
			
			//cout << sum << " ";
		}
		
		cout << bestLeft << " " << bestRight << endl;
		
	}
	
}
