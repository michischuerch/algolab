// lestrade

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <vector>
#include <map>
#include <algorithm>
#include <set>

// example: find the risk-optimal investment strategy
// in Swatch and Credit Suisse shares (data as in lecture)
#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose exact rational type
#include <CGAL/Gmpq.h>


using namespace std;


typedef CGAL::Gmpq ET;
// solution type the solver provides
typedef CGAL::Quotient<ET> SolT;
// program and solution types
typedef CGAL::Quadratic_program<long> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Vertex_iterator  Vertex_iterator;
typedef std::map<K::Point_2,long > CostMap;
typedef std::map<K::Point_2,pair<K::FT,int> > AgentToDistance;

/*double floor_to_double(auto x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

long ceil_to_long(auto x) {
	return -long(floor_to_double(-x));
}*/

int main()
{
	int t; cin>>t;
	while(t--) {
		int z,u,v,w;cin>>z>>u>>v>>w; // holmes fee, where, who, how
		int a,g;cin>>a>>g; //agents, gang members
		
		CostMap cm;
		AgentToDistance atd;
		
		std::vector<vector<long>> gangMembers(g);
		for(int j=0;j<g;j++) {
			long x,y,u,v,w;cin>>x>>y>>u>>v>>w;
			vector<long> newMember{x,y,u,v,w};
			gangMembers[j] = newMember;
		}
		
		std::vector<K::Point_2> pts;
		pts.reserve(a);
		for (std::size_t i = 0; i < a; ++i) {
			long x,y,z; cin>>x>>y>>z;
			K::Point_2 p(x,y);
			pts.push_back(p);
			cm[p] = z;
			atd[p] = make_pair(LONG_MAX,0);
		}
		
		// construct triangulation
		Triangulation t;
		t.insert(pts.begin(), pts.end());
		//for (Face_iterator f = t.finite_faces_begin(); f != t.finite_faces_end(); ++f)
		
		for(int j=0;j<g;j++) {
			K::Point_2 p(gangMembers[j][0],gangMembers[j][1]);
			K::Point_2 o = t.nearest_vertex(p)->point();
			K::FT sd = CGAL::squared_distance(p,o);
			if(atd[o].first > sd) atd[o] = make_pair(sd,j);
		}
		
		vector<long> gangBestWatcherCost(g,LONG_MAX);
		for (Vertex_iterator v = t.finite_vertices_begin(); v != t.finite_vertices_end(); ++v) {
			int j = atd[v->point()].second;
			gangBestWatcherCost[j] = min(gangBestWatcherCost[j], cm[v->point()]);
		}
		
		vector<vector<long>> observed; 
		
		int lul = 0;
		for(int j=0;j<g;j++) {
			if(gangBestWatcherCost[j]!=LONG_MAX) {
				lul++;
				vector<long> newO{gangMembers[j][2],gangMembers[j][3],gangMembers[j][4],gangBestWatcherCost[j]};
				observed.push_back(newO);
			}
		}
		cout << lul << "\n";
		continue;
		/*for (auto it = pm.begin(); it != pm.end(); ++it) {
			if(it->second[3]!=LONG_MAX) observed.push_back(it->second);
		}*/
		//cout << observed.size() << "\n";
		// Linear Program
		Program lp (CGAL::LARGER, true, 0, true, 24); 
		for(int i=0;i<observed.size();i++) {
			lp.set_a(i, 0, observed[i][0]);  
			lp.set_a(i, 1, observed[i][1]);
			lp.set_a(i, 2, observed[i][2]);
			lp.set_c(i, observed[i][3]);  
		}
		lp.set_b(0, u);
		lp.set_b(1, v);
		lp.set_b(2, w);
		
		Solution s = CGAL::solve_linear_program(lp, ET());
		//assert (s.solves_linear_program(lp));	
		
		if (s.status() == CGAL::QP_INFEASIBLE) cout << "H";
		else if(s.objective_value() <= z) cout << "L";
		else cout << "H";
		
		/*cout << " ";
		
		CGAL::Quadratic_program_solution<ET>::Variable_value_iterator
			opt = s.variable_values_begin(); 
		
			for(int i=0;i<observed.size();i++) {
				cout << ceil_to_long(*(opt+i)) << " ";
			}*/
			/*long smallestPay = 0;
		
			CGAL::Quadratic_program_solution<ET>::Variable_value_iterator
			opt = s.variable_values_begin(); 
		
			for(int i=0;i<observed.size();i++) {
				//cout << ceil_to_long(*(opt+i)) << " ";
				smallestPay += ceil_to_long(*(opt+i)) * observed[i][3];
			}
			smallestPay <= z  ? cout << "L" : cout << "H";
		}*/
		cout << "\n";
		//cout <<  << "\n";
	}
}
