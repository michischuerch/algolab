// lestrade

// idea: use delaunay triangulation to find nearest gangMember for each agent
// update the cost of a gangMember to the cost of the cheapest agent observing (in the map)
// then use linear programming with the 3 unequalities and try to minimize the cost

// shit of the day: solve_nonnegative_linear_program can somehow not have an upper bound on the Program (would need to add alot uf uneq...)
// TIL: - using Gmpz (integer) instead of Gmpq (quotient) is much faster, 
//      - always fucking use "ios_base::sync_with_stdio(false);"! (70->100 points)
//      - cautious with the slides! LP slides are garbage -> little information, hard to find what i want, and also always changing ineq (confusing as fuck) -> caution with CGAL::SMALLER / CGAL::LARGER
//      - to optimize over multiple variables LP is a good choice 



#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <vector>
#include <map>
#include <algorithm>

// example: find the risk-optimal investment strategy
// in Swatch and Credit Suisse shares (data as in lecture)
#include <iostream>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose exact rational type
#include <CGAL/Gmpz.h>


using namespace std;


typedef CGAL::Gmpz ET;
// solution type the solver provides
typedef CGAL::Quotient<ET> SolT;
// program and solution types
typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef std::map<K::Point_2,vector<long> > PointMap;

int main()
{
	ios_base::sync_with_stdio(false);
	int t; cin>>t;
	while(t--) {
		int z,u,v,w;cin>>z>>u>>v>>w; // holmes fee, where, who, how
		int a,g;cin>>a>>g; //agents, gang members
		
		PointMap pm;
		
		std::vector<K::Point_2> pts;
		pts.reserve(g);
		for(int j=0;j<g;j++) {
			K::Point_2 p;
			std::cin >> p;
			pts.push_back(p);
			vector<long> data(3);
			for(int i=0;i<3;i++) cin>>data[i];
			data.push_back(LONG_MAX); // not observed at start, infinite cost
			pm[p] = data;
		}
		// construct triangulation
		Triangulation t;
		t.insert(pts.begin(), pts.end());
		
		for(int j=0;j<a;j++) {
			long x,y,z; cin>>x>>y>>z;
			K::Point_2 p(x,y);
			K::Point_2 o = t.nearest_vertex(p,t.locate(p))->point();
			pm[o][3] = min(pm[o][3],z);
		}
		
		vector<vector<long>> observed; 
		
		for (auto it = pm.begin(); it != pm.end(); ++it) {
			if(it->second[3]!=LONG_MAX) observed.push_back(it->second);
		}
		// Linear Program
		Program lp (CGAL::SMALLER, true, 0, true, 24); 
		for(int i=0;i<observed.size();i++) {
			lp.set_a(i, 0, -observed[i][0]);  
			lp.set_a(i, 1, -observed[i][1]);
			lp.set_a(i, 2, -observed[i][2]);
			lp.set_c(i, observed[i][3]);  
		}
		lp.set_b(0, -u);
		lp.set_b(1, -v);
		lp.set_b(2, -w);
		
		Solution s = CGAL::solve_linear_program(lp, ET());
		
		if (s.status() == CGAL::QP_INFEASIBLE) cout << "H";
		else if(s.objective_value() <= z) cout << "L";
		else cout << "H";
		
		cout << "\n";
	}
}
