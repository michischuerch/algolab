// graypes

// realized that the solution is just the shortest distance between grapes. lul

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef std::map<Triangulation::Vertex_handle,Triangulation::Vertex_handle> VertexMap;

using namespace std;

double floor_to_double(const K::FT& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

int main()
{
	int n; cin>>n;
	while(n!=0) {
		std::vector<K::Point_2> pts;
		pts.reserve(n);
		for (std::size_t i = 0; i < n; ++i) {
			K::Point_2 p;
			std::cin >> p;
			pts.push_back(p);
		}
		// construct triangulation
		Triangulation t;
		t.insert(pts.begin(), pts.end());
		
		K::FT d(LONG_MAX);
		for (Edge_iterator e = t.finite_edges_begin(); e != t.finite_edges_end(); ++e) {
			Triangulation::Vertex_handle v1 = e->first->vertex((e->second + 1) % 3);
			Triangulation::Vertex_handle v2 = e->first->vertex((e->second + 2) % 3);
			d=min(d,t.segment(e).squared_length());
		}
		cout <<fixed<< long(-floor_to_double(-(CGAL::sqrt(d)*50))) << "\n";
		cin>>n;
	}
	return 0;
}
