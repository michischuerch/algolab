// graypes

// 1. for each point find closest point, 2. see if two points go to each other for each edge. 
// useful stuff: Edge_iterator, Vertex_iterator, Circulators. to see if to handles are equal use "v1->point() == v2.point()". this works

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <map>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Vertex_iterator  Vertex_iterator;
typedef std::map<Triangulation::Vertex_handle,Triangulation::Vertex_handle> VertexMap;

using namespace std;

double floor_to_double(const K::FT& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

int main()
{
	int n; cin>>n;
	while(n!=0) {
		std::vector<K::Point_2> pts;
		pts.reserve(n);
		for (std::size_t i = 0; i < n; ++i) {
			K::Point_2 p;
			std::cin >> p;
			pts.push_back(p);
		}
		// construct triangulation
		Triangulation t;
		t.insert(pts.begin(), pts.end());
		VertexMap vertexMap;
		
		for (Vertex_iterator v = t.finite_vertices_begin(); v != t.finite_vertices_end(); ++v) {
			 Triangulation::Edge_circulator c = t.incident_edges(v);
			 K::FT d(LONG_MAX);
			 Triangulation::Vertex_handle runTo = v;
			 do {
				if (!t.is_infinite(c)) {
					Triangulation::Edge e = *c;
					Triangulation::Vertex_handle v1 = e.first->vertex((e.second + 1) % 3);
					Triangulation::Vertex_handle v2 = e.first->vertex((e.second + 2) % 3);
					
					if(v2->point()==v->point()) swap(v1,v2);
					
					if(d>t.segment(c).squared_length()) {
						runTo = v2;
						d=t.segment(c).squared_length();
					}
					else if(d==t.segment(c).squared_length()) {
						if(runTo->point().x() > v2->point().x() || (runTo->point().x() == v2->point().x() && runTo->point().y() > v2->point().y())) runTo = v2;
					}
				}
			 } while (++c != t.incident_edges(v));
			 vertexMap[v] = runTo;
		}
		
		K::FT d(LONG_MAX);
		for (Edge_iterator e = t.finite_edges_begin(); e != t.finite_edges_end(); ++e) {
			Triangulation::Vertex_handle v1 = e->first->vertex((e->second + 1) % 3);
			Triangulation::Vertex_handle v2 = e->first->vertex((e->second + 2) % 3);
			if(vertexMap[v1]->point() == v2->point() && vertexMap[v2]->point() == v1->point()) d=min(d,t.segment(e).squared_length());
		}
		cout <<fixed<< long(-floor_to_double(-(CGAL::sqrt(d)*50))) << "\n";
		cin>>n;
	}
	return 0;
}
