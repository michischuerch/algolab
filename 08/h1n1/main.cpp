// h1n1

// fuck up of the day: vergesse zwösche samples de map zrugg setze ... lul

// cooli funktion: t.locate(Point) git s'Face wo de punkt denne isch
// CGAL::squared_distace + CGAL::nearest_vertex useful

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <map>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::All_faces_iterator All_faces_iterator;
typedef std::map<Triangulation::Face_handle,bool> FaceDone;

using namespace std;

bool recursion(Triangulation &t, const Triangulation::Face_handle &fh, long d, FaceDone &fd) {
	fd[fh] = true;
	if(t.is_infinite(fh)) return true;
	else {
		// go over all edges of the face and check where we can go...
		for(int i=0;i<3;i++) {
			if(!fd[fh->neighbor(i)]) {
				K::Point_2 n1 = fh->vertex((i+1)%3)->point();
				K::Point_2 n2 = fh->vertex((i+2)%3)->point();
				
				if(CGAL::squared_distance(n1,n2)/4 >= d && recursion(t,fh->neighbor(i),d,fd)) return true;
			}
		}
	}
	return false;
}
	
bool escaping(Triangulation &t, K::Point_2 &q, long d, FaceDone &fd) {
	// check initial position
	if(CGAL::squared_distance(t.nearest_vertex(q)->point(),q) < d) return false;
	Triangulation::Face_handle start = t.locate(q);
	return recursion(t,start,d,fd);

}

int main()
{
	int n,m;cin>>n;
	while(n!=0) {
		std::vector<K::Point_2> pts;
		pts.reserve(n);
		for (std::size_t i = 0; i < n; ++i) {
			int x,y; cin>>x>>y;
			K::Point_2 p(x,y);
			pts.push_back(p);
		}
		// construct triangulation
		Triangulation t;
		t.insert(pts.begin(), pts.end());
		
		cin>>m;
		for(int j=0;j<m;j++) {
			int x,y;cin>>x>>y;
			K::Point_2 q(x,y);
			long d; cin>>d;
			// reset, lul vergesse am aafang
			//this is to slow! -> for (All_faces_iterator f = t.all_faces_begin(); f != t.all_faces_end(); ++f) fd[f] = false;
			FaceDone fd;
			escaping(t,q,d,fd) ? cout<<"y" : cout<<"n";
		}
		cout<<"\n";
		
		cin>>n;
	}
}
