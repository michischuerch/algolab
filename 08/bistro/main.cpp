// bistro

// learnt: insert points with an array (much more efficient)
// to not use scientific notation use "cout<<std::fixed"

#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Finite_faces_iterator  Face_iterator;

double floor_to_double(const K::FT& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

int main()
{
	ios_base::sync_with_stdio(false);
	// read number of points
	int n,m;
	std::cin >> n;
	std::vector<K::Point_2> pts;
	Triangulation t;
	while(n!=0) {
		
		pts.clear();
		pts.reserve(n);
		t.clear();
		
		// construct triangulation
		for (int i = 0; i < n; ++i) {
			K::Point_2 p;
			std::cin >> p;
			pts.push_back(p);
		}
		t.insert(pts.begin(), pts.end());
		cin >> m;
		for(int j=0;j<m;j++) {
			K::Point_2 p;
			cin >> p;
			K::Segment_2 seg(p,t.nearest_vertex(p)->point());
			cout << fixed << long(floor_to_double(seg.squared_length()))  << "\n";
		}
		cin >> n;
	}
}
