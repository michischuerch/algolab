// germs

// realised that we have to consider that to the boundary the formula is "t=sqrt(x-0.5)" and between two cells it is "t=sqrt(0.5*x-1)=0.25 * sqrt(x-0.5), where x is the distance the cell can maximally grow to
// another thing is that the inexact construction kernel seems to be increasing the speed of predicates as well. -> try t use it as much as possible
// and i learnt about nth_element-magic, where we can find out pretty fast where the nth element in an array is

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <climits>
#include <algorithm>
#include <iostream>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_2<K> Vb;
typedef CGAL::Triangulation_face_base_2<Vb> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Vertex_iterator  Vertex_iterator;

// copy from cgal codesnippet...
double floor_to_double(const K::FT& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}


double squared_distance_to_time(const K::FT& x)
{
	K::FT time;
	if(x <= 0 ) time = 0;
	else time = CGAL::sqrt(CGAL::sqrt(x)-0.5);
	
	// general trick to ceil -> ceil = -floor(-x)
	return -(floor_to_double(-time));
}

int main()
{
	int n; cin>>n;
	while(n!=0) {
		// these lines can be found in the 2. example on the judge
		// just copy paste
		int l,b,r,tt;cin>>l>>b>>r>>tt;
		vector<K::Point_2> pts(n);
		for (int i = 0; i < n; ++i) {
			int p1,p2;cin >>p1>>p2;
			pts[i] = K::Point_2(p1,p2);
		}
		Triangulation t;
		t.insert(pts.begin(), pts.end());
		
		// iterate over all vertices, distance is min distance to boundaries or to all adjacent edges
		// it seems like the circulator can be 0, however we can check this before using it...
		vector<K::FT> shortestDistances;
		for (Vertex_iterator v = t.finite_vertices_begin(); v != t.finite_vertices_end(); ++v) {
			K::FT d; 
			d = pow(v->point().x()-l,2);
			d = min(d,pow(-v->point().x()+r,2));
			d = min(d,pow(v->point().y()-b,2));
			d = min(d,pow(tt-v->point().y(),2));
			
			Triangulation::Edge_circulator c = t.incident_edges(v);
			if(c!=0) {			
				do {
					if (!t.is_infinite(c)) d = min(d,t.segment(c).squared_length()/4);
				} while (++c != t.incident_edges(v));
			}
			shortestDistances.push_back(d);
		}
		
		// we only want to find items at specific positions in the array
		// therefore we only sort the items of interest
		// note that we know after searching for example for the middle element that all elements on the left of the middle are smaller and therefore we can look in the second half for the big element!
		nth_element(shortestDistances.begin(), shortestDistances.begin(),shortestDistances.end());
		K::FT mini = shortestDistances[0];
		nth_element(shortestDistances.begin()+1, shortestDistances.begin()+n/2,shortestDistances.end());
		K::FT midi = shortestDistances[n/2];
		nth_element(shortestDistances.begin()+n/2+1, shortestDistances.end()-1,shortestDistances.end());
		K::FT maxi = shortestDistances[n-1];
		
		cout << abs(squared_distance_to_time(mini)) << " " 
				<< abs(squared_distance_to_time(midi)) << " " 
				<< abs(squared_distance_to_time(maxi)) << "\n";
		
		cin>>n;
	}
	return 0;
}
