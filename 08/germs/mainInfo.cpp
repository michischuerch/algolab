// germs

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <climits>
#include <algorithm>
#include <iostream>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<K::FT, K> Vb;
//typedef CGAL::Triangulation_vertex_base_2<K> Vb;
typedef CGAL::Triangulation_face_base_2<Vb> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Vertex_iterator  Vertex_iterator;
//typedef std::unordered_map<Triangulation::Vertex_handle,K::FT> DistanceMap;

double floor_to_double(const K::FT& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}


double squared_distance_to_time(const K::FT& x)
{
	K::FT time;
	if(x <= 0 ) time = 0;
	else time = CGAL::sqrt(CGAL::sqrt(x)-0.5);
	
	return -(floor_to_double(-time));
}

int main()
{
	int n; cin>>n;
	while(n!=0) {
		
		int l,b,r,tt;cin>>l>>b>>r>>tt;
		vector<K::Point_2> pts(n);
		//pts.reserve(n);
		for (int i = 0; i < n; ++i) {
			int p1,p2;cin >>p1>>p2;
			pts[i] = K::Point_2(p1,p2);
		}
		
		 // construct triangulation
		Triangulation t;
		t.insert(pts.begin(), pts.end());
		
		vector<K::FT> shortestDistances;
		
		
		
		for (Vertex_iterator v = t.finite_vertices_begin(); v != t.finite_vertices_end(); ++v) {
			K::FT d; 
			d = pow(v->point().x()-l,2);
			d = min(d,pow(-v->point().x()+r,2));
			d = min(d,pow(v->point().y()-b,2));
			d = min(d,pow(tt-v->point().y(),2));
			//distMap[v] = d;
			v->info() = d;
		}
		for (Edge_iterator e = t.finite_edges_begin(); e != t.finite_edges_end(); ++e) {
			Triangulation::Vertex_handle v1 = e->first->vertex((e->second + 1) % 3);
			Triangulation::Vertex_handle v2 = e->first->vertex((e->second + 2) % 3);
			
			K::FT d = t.segment(e).squared_length()/4;
			v1->info() = min(v1->info(), d);
			v2->info() = min(v2->info(), d);
		}
		for (Vertex_iterator v = t.finite_vertices_begin(); v != t.finite_vertices_end(); ++v) {
			shortestDistances.push_back(v->info());
		}
		nth_element(shortestDistances.begin(), shortestDistances.begin()+n/2,shortestDistances.end());
		K::FT midi = shortestDistances[n/2];
		
		auto mini = min_element(shortestDistances.begin(),shortestDistances.end());
		auto maxi = max_element(shortestDistances.begin(),shortestDistances.end());
		
		cout << abs(squared_distance_to_time(*mini)) << " " 
				<< abs(squared_distance_to_time(midi)) << " " 
				<< abs(squared_distance_to_time(*maxi)) << "\n";
		
		cin>>n;
	}
	return 0;
}
