// carsharing

// ALGOLAB BGL Tutorial 3
// Code snippets demonstrating 
// - MinCostMaxFlow with arbitrary edge costs using cycle_canceling
// - MinCostMaxFlow with non-negative edge costs using successive_shortest_path_nonnegative_weights

// Compile and run with one of the following:
// g++ -std=c++11 -O2 bgl_mincostmaxflow.cpp -o bgl_mincostmaxflow; ./bgl_mincostmaxflow
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 bgl_mincostmaxflow.cpp -o bgl_mincostmaxflow; ./bgl_mincostmaxflow

// Includes
// ========
// STL includes
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace std;

// BGL Graph definitions
// ===================== 
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = c;
        weightmap[e] = w; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -w; // new
        revedgemap[e] = rev_e; 
        revedgemap[rev_e] = e; 
    }
};

int testcases() {
	int N,S;cin>>N>>S;
	
	vector<int> l(S);
	long numCars = 0;
	for(int i=0;i<S;i++) {
		cin>>l[i];
		numCars += l[i];
	}
	
	vector<vector<long>> bookingRequests;
	vector<vector<int>> times(S); // [[time]]
	long maxP = 0;
	long maxT = 0;
	for(int i=0;i<N;i++) {
		long s,t,d,a,p; cin>>s>>t>>d>>a>>p;
		times[s-1].push_back(d);
		times[t-1].push_back(a);
		vector<long> request = {s-1,t-1,d,a,p};
		bookingRequests.push_back(request);
		maxP = max(maxP,p);
		maxT = max(maxT,a);
	}
	
	for(int i=0;i<times.size();i++) {
		sort(times[i].begin(),times[i].end());
	}
	
	
	vector<map<int,int>> id(S); // for each station a map for the times
	vector<long> starts;
	vector<long> ends;
	long graphSize = 0;
	for(int i=0;i<times.size();i++) { 
		vector<int> t = times[i];
		starts.push_back(graphSize);
		int counter = 0;
		for(auto iter = t.begin(); iter != t.end(); iter++) {
			id[i][*iter] = graphSize+counter;
			counter++;
		}
		ends.push_back(graphSize+t.size()-1);
		graphSize += t.size();
	} 
	
	int sss = graphSize;
	int ttt = graphSize+1;
	
    // Create Graph and Maps
    Graph G(2+graphSize);
    EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
    EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
    ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
    ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);
    
    int c = 0;
    for(int i=0;i<S;i++) {
		
		int current;
		int last = *(times[i].begin());
		
		if(times[i].size() == 0) {
			eaG.addEdge(sss, ttt, l[i], maxP*(maxT));
			continue;
		}
		
		eaG.addEdge(sss, c, l[i], maxP*(last));
		
		for(auto iter = next(times[i].begin());iter != times[i].end();iter++) {
			current = *iter;
			eaG.addEdge(c, c+1, numCars, maxP*(current-last));
			last = current;
			c++;
		}
		
		eaG.addEdge(c, ttt, numCars, maxP*(maxT-(*(times[i].rbegin()))));
		c++;
	}
	
	for(vector<long> r : bookingRequests) {
		int from = id[r[0]][r[2]];
		int to = id[r[1]][r[3]];
		eaG.addEdge(from, to, 1, -r[4]+maxP*(r[3]-r[2]));
	}
    // Option 2: Min Cost Max Flow with successive_shortest_path_nonnegative_weights
    boost::successive_shortest_path_nonnegative_weights(G, sss, ttt);
    long cost = boost::find_flow_cost(G);
    cout << maxT*maxP*numCars - cost;	
	cout << "\n";
    return 0;
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
