// defensive line

// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>
#include <map>

using namespace std;

struct Interval {
	int left;
	int right;
};

bool doesOverlap(Interval i1, Interval i2) {
	if((i1.left <= i2.right)) return true;
	else return false;
}

int testcases() {
	int n,m,k; cin>>n>>m>>k;
	vector<int> v(n);
	for(int i=0;i<n;i++) cin>>v[i];
	
	int left = 0, right = 0;
	int sum = v[0];
	
	vector<Interval> intervals; // [(l,r)]
	while(right < n) {
		if(sum < k) {
			right++;
			sum+=v[right];
		}
		else if(sum > k){
			sum-=v[left];
			left++;
		}
		else if(sum == k) {
			Interval newInterval = {left,right};
			intervals.push_back(newInterval);
			right++;
			sum+=v[right];
		}
	}
	
	vector<vector<int>> validSubsets; // [(#attackers, value, lastLeft, lastRight)]
	vector<int> init = {0,0,-1,-1};
	validSubsets.push_back(init);
	int best = 0;
	//map<Interval,pair<int,int>> intervalValueMap;
	// note that intervals are sorted already
	for(int i=0;i<intervals.size();i++) {
		Interval check = intervals[i];
		
		//cout << "(" << check.left << " " << check.right << ") ";
		
		for(int j=0;j<validSubsets.size();j++) {
			int attackers = validSubsets[j][0];
			int value = validSubsets[j][1];
			Interval interval = {validSubsets[j][2],validSubsets[j][3]};
			
			if(!doesOverlap(check,interval)) {
				if(attackers+1==m) best=max(best,value+check.right-check.left+1);
				else {
					vector<int> newSubset = {attackers+1,value + check.right-check.left+1, check.left,check.right};
					validSubsets.push_back(newSubset);
				}
				//cout << "("<<attackers+1 << " " << value + check.right-check.left+1 << " " << check.left << " " << check.right << ") ";
			}
		}
	}
	if(best == 0) cout << "fail";
	else cout << best;
	cout << "\n";
	
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
