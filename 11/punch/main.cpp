// punch

// ALGOLAB BGL Tutorial 3
// Code snippets demonstrating 
// - MinCostMaxFlow with arbitrary edge costs using cycle_canceling
// - MinCostMaxFlow with non-negative edge costs using successive_shortest_path_nonnegative_weights

// Compile and run with one of the following:
// g++ -std=c++11 -O2 bgl_mincostmaxflow.cpp -o bgl_mincostmaxflow; ./bgl_mincostmaxflow
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 bgl_mincostmaxflow.cpp -o bgl_mincostmaxflow; ./bgl_mincostmaxflow

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>

using namespace std;

int testcases() {
    int n,k; cin>>n>>k;
    vector<int> cs(n+1);
    vector<int> vs(n+1);
    int maxV=0;
    for(int i=1;i<=n;i++) {
		cin>>cs[i];
		cin>>vs[i];
		maxV=max(maxV,vs[i]);
	}
	vector<vector<pair<int,int>>> dp(n+1); // n x k (minCost,maxBeverages)
	for(int i=0;i<=n;i++) {
		vector<pair<int,int>> dp_i(k+maxV+1,make_pair(INT_MAX,0));
		dp_i[0].first=0;
		dp[i]=dp_i;
	}
	
	for(int i=1;i<=n;i++) {
		for(int j=1;j<=k+maxV;j++) {
			int minCost = dp[i-1][j].first;
			int maxAmount = dp[i-1][j].second; 
			
			if(j>=vs[i]) {
				// case 1
				int p = j-vs[i];
				int cost1 = cs[i]+dp[i-1][p].first;
				if(dp[i-1][p].first!=INT_MAX) {
					if(minCost > cost1) {
						minCost = cost1;
						maxAmount = dp[i-1][p].second+1;
					} else if(minCost == cost1) {
						maxAmount = max(maxAmount,dp[i-1][p].second+1);
					}
				}
				
				// case 2
				int cost2 = cs[i]+dp[i][p].first;
				if(dp[i][p].first!=INT_MAX) {
					if(minCost > cost2) {
						minCost = cost2;
						maxAmount = dp[i][p].second;
					} else if(minCost == cost2) {
						maxAmount = max(maxAmount,dp[i][p].second);
					}
				}
			}
			
			dp[i][j] = make_pair(minCost,maxAmount);
		}
	}
	
	int minC = INT_MAX;
	int maxA = 0;
	for(int j=k;j<k+maxV;j++) {
		if(minC > dp[n][j].first) {
			minC = dp[n][j].first;
			maxA = dp[n][j].second;
		} else if(minC == dp[n][j].first) {
			maxA = max(maxA,dp[n][j].second);
		}
	}
	
	cout << minC << " " << maxA;
	
	cout << "\n";
    return 0;
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
