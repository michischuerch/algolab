// return of the jedi

// idea: solution is minimum spanning tree with one edge that gives the lowest addition to the "cycle" that occurs when adding to the MST.
// 1. calc MST using kruskal / prim
// 2. use MST to find all neighboring vertices, save it in vector list "neigh"
// 3. use BFS from every node to find the maximum edge between two vertices (saved in query[u,v])
// 4. solution is the minimum of an edge not in MST that has weight w - query[u,v]

// ALGOLAB BGL Tutorial 1
// Code snippets demonstrating 
// - graph definitions
// - several algorithms (components, distance-based algorithms, maximum matching)
// - how to pass exterior property maps
// - use of iterators

// Compile and run with one of the following:
// g++ -std=c++11 -O2 bgl-code_snippets.cpp -o bgl-code_snippets; ./bgl-code_snippets
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 bgl-code_snippets.cpp -o bgl-code_snippets; ./bgl-code_snippets

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t


using namespace std;

int testcases() {
	int n,start; cin>>n>>start;
	
	Graph G(n);	// creates an empty graph on n vertices
	WeightMap weightmap = boost::get(boost::edge_weight, G);	// start by defining property maps for all interior properties defined in Lines 37, 38
	
	for(int j=1;j<n;j++) {
		for(int k = 1;k<n-j+1;k++) {
			Edge e;	bool success;
			int c; cin>>c;
			
			boost::tie(e, success) = boost::add_edge(j-1, j+k-1, G);
			weightmap[e] = c;
		}
	}
	
	// Kruskal minimum spanning tree
	// =============================
	std::vector<Edge>	mst; // We must use this vector to store the MST edges (not as a property map!)
	// We can use the following vectors as Exterior Property Maps if we want to access additional information computed by Union-Find:	
	std::vector<Vertex>	kruskalpredmap(n);	// Stores predecessors needed for Union-Find (NOT the MST!)
	std::vector<int>	rankmap(n);		// Stores ranks needed for Union-Find
	boost::kruskal_minimum_spanning_tree(G, std::back_inserter(mst),	// kruskal_minimum_spanning_tree(G, back_inserter(mst)); would be fine as well 
			boost::rank_map(boost::make_iterator_property_map(rankmap.begin(), boost::get(boost::vertex_index, G))).
			predecessor_map(boost::make_iterator_property_map(kruskalpredmap.begin(), boost::get(boost::vertex_index, G))));			
	int totalweight = 0;
	vector<vector<pair<int,int>>> neigh(n); //[(id,c)]
	std::set<Edge> mstSet;
	// go through the minimum spanning tree with an iterator
	std::vector<Edge>::iterator	mstbeg, mstend = mst.end();
	for (mstbeg = mst.begin(); mstbeg != mstend; ++mstbeg) {
		mstSet.insert(*mstbeg);
		totalweight += weightmap[*mstbeg];
		Vertex u = boost::source(*mstbeg, G);
		Vertex v = boost::target(*mstbeg, G);
		int w = weightmap[*mstbeg];
		
		neigh[u].push_back(make_pair(v,w));
		neigh[v].push_back(make_pair(u,w));
	}
	
	vector<vector<int>> query;
	for(int src=0;src<n;src++) {
		
		vector<int> queryVector(n,0);
		
		// BFS to find vertex set S
		std::vector<int> vis(n, false); // visited flags
		std::queue<int> Q;
		vis[src] = true; // Mark the source as visited
		Q.push(src);
		while (!Q.empty()) {
			const int u = Q.front();
			Q.pop();
			for(pair<int,int> p : neigh[u]) {
				if (vis[p.first]) continue;
				vis[p.first] = true;
				queryVector[p.first] = max(p.second,queryVector[u]);
				Q.push(p.first);
			}
		}
		query.push_back(queryVector);
	}
	
	
	int solution = INT_MAX;
	EdgeIt ebeg, eend;
	for (boost::tie(ebeg, eend) = boost::edges(G); ebeg != eend; ++ebeg) {	// edges(G) returns a pair of iterators which define a range of all edges. 
		
		auto search = mstSet.find(*ebeg);
		if(search == mstSet.end()) {
			// For undirected graphs, each edge is visited once, with some orientation.
			// ebeg is EdgeIterator, *ebeg is EdgeDescriptor
			Vertex u = source(*ebeg, G);
			Vertex v = target(*ebeg, G);
			int w = weightmap[*ebeg];
			
			solution = min(solution,w-query[u][v]);
		}
	}
	
	cout << totalweight + solution;
	cout << "\n";
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
