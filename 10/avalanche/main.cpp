// avalanche

// ALGOLAB BGL Tutorial 3
// Code snippets demonstrating 
// - MinCostMaxFlow with negative edge costs using cycle_canceling
// - MinCostMaxFlow with negative edge costs using successive_shortest_path_nonnegative_weights

// Compile and run with one of the following:
// g++ -std=c++11 -O2 bgl_mincostmaxflow.cpp -o bgl_mincostmaxflow; ./bgl_mincostmaxflow
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 bgl_mincostmaxflow.cpp -o bgl_mincostmaxflow; ./bgl_mincostmaxflow

// Includes
// ========
// STL includes
#include <iostream>
#include <cstdlib>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>
// Namespaces
using namespace boost;
using namespace std;

// BGL Graph definitions
// ===================== 
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > Graph;
// Interior Property Maps
typedef property_map<Graph, edge_capacity_t>::type      EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResidualCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          Vertex;
typedef graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;
typedef graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacitymap[e] = c;
        weightmap[e] = w;
        capacitymap[reverseE] = 0;
        weightmap[reverseE] = -w;
        revedgemap[e] = reverseE; 
        revedgemap[reverseE] = e; 
    }
    void addEdgeLight(int u, int v, long w) {
        Edge e;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        //tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        weightmap[e] = w;
    }
};

int testcases() {
	int n,m,a,s,c,d; cin>>n>>m>>a>>s>>c>>d;

    // Create Graph and Maps
    Graph G(n);
    EdgeCapacityMap capacitymap = get(edge_capacity, G);
    EdgeWeightMap weightmap = get(edge_weight, G);
    ReverseEdgeMap revedgemap = get(edge_reverse, G);
    ResidualCapacityMap rescapacitymap = get(edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);
    
    for(int j=0;j<m;j++) {
		char w;
		int x,y,z; cin>>w>>x>>y>>z;
		if(w=='S') {
			eaG.addEdgeLight(x,y,z);
		} else {
			eaG.addEdgeLight(x,y,z);
			eaG.addEdgeLight(y,x,z);
		}
	}
    
    for(int i=0;i<a;i++) {
		int ai; cin>>ai;
		eaG.addEdgeLight(n+i,ai,0);
	}
	
	for(int i=0;i<s;i++) {
		int si; cin>>si;
		eaG.addEdgeLight(si,n+a+i,d);
		if(c==2) eaG.addEdgeLight(si,n+a+s+i,2*d);
	}
	s*=c;
	
	
	int sss = n+a+s;
	int ttt = n+a+s;
	
    //int cost = find_flow_cost(G);
	
	/*vector<int> lsDist(a,0);
	vector<int> predMap(n+2);
	OutEdgeIt e, eend;*/
	
	// Create Graph and Maps
    
	
	/*EdgeIt e,ebeg, eend;
	for (boost::tie(ebeg, eend) = boost::edges(G); ebeg != eend; ++ebeg) {
		const int u = source(*ebeg, G);
		const int v = target(*ebeg, G);
		//cout<<rescapacitymap[*ebeg] << " ";
		if(rescapacitymap[*ebeg] <= a) //for(int i=0;i<rescapacitymap[*ebeg];i++) {
			cout << "(" << u << "," << v << ") ";
			//if(weightmap[*ebeg]>=0) eaG2.addEdgeLight(u,v,weightmap[*ebeg]);
		//}
	}*/
	
	/*for(int i=0;i<shelters.size();i++) eaG2.addEdgeLight(shelters[i],n+i,d);
	
	for(boost::tie(e, eend) = boost::out_edges(boost::vertex(sss,G), G); e != eend; ++e) {
		const int v = target(*ebeg, G);
		eaG2.addEdgeLight(,1,rescapacitymap[*ebeg]);
	}*/
	vector<vector<int>> distmaps;
	for(int i=0;i<a;i++) {
		std::vector<Vertex> predmap(n+a+s);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
		std::vector<int> distmap(n+a+s);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
		boost::dijkstra_shortest_paths(G, n+i, // We MUST provide at least one of the two maps
			boost::predecessor_map(boost::make_iterator_property_map(predmap.begin(), boost::get(boost::vertex_index, G))).	// predecessor map as Named Parameter
			distance_map(boost::make_iterator_property_map(distmap.begin(), boost::get(boost::vertex_index, G))));	// distance map as Named Parameter
		
		//std::vector<int> d2 ;
		for(int j=0;j<s;j++) {
			vector<int> data(3);
			data[0] = distmap[n+a+j];
			data[1] = i;
			data[2] = j;
			distmaps.push_back(data);
		}
		
	}
	sort(distmaps.begin(),distmaps.end());
	
	/*Graph G2(a+s+2);
    EdgeCapacityMap capacitymap2 = get(edge_capacity, G2);
    EdgeWeightMap weightmap2 = get(edge_weight, G2);
    ReverseEdgeMap revedgemap2 = get(edge_reverse, G2);
    ResidualCapacityMap rescapacitymap2 = get(edge_residual_capacity, G2);
    EdgeAdder eaG2(G2, capacitymap2, weightmap2, revedgemap2);
    
    for(int i=0;i<a;i++) {
		for(int j=0;j<distmaps[i].size();j++) {
			//cout << distmaps[i][j] << " ";
			if(distmaps[i][j] < INT_MAX) eaG2.addEdge(i,a+j,1,distmaps[i][j]);
		}
		//cout << "| ";
	}
    
    for(int i=0;i<a;i++) {
		eaG2.addEdge(a+s,i,1,0);
	}
	for(int j=0;j<s;j++) {
		eaG2.addEdge(a+j,a+s+1,1,0);
	}
	
	
	boost::successive_shortest_path_nonnegative_weights(G2, a+s, a+s+1);
	//int solution = boost::find_flow_cost(G2);
	long solution = 0;
	OutEdgeIt e, eend;
	for(int i=0;i<a;i++) {
		for(boost::tie(e, eend) = boost::out_edges(boost::vertex(i,G2), G2); e != eend; ++e) {
			if(rescapacitymap2[*e] > 0) {
				const int u = source(*e, G2);
				const int v = target(*e, G2);
				cout << "(" << u << "," << v<< ") ";
				solution = max(solution, weightmap2[*e]);
			}
		}
	}*/
	
	int numActiveA = s;
	//vector<int> activeS(s,-1);
	vector<int> activeS(s,a);
	//vector<int> aTime(a);
	//vector<int> ps(a,0);
	
	//int lastTime = 0;
	for(int i=distmaps.size()-1;i>=0;i--) {
		int time = distmaps[i][0];
		int ii = distmaps[i][1];
		int jj = distmaps[i][2];
		activeS[jj]--;
		if(activeS[jj] == 0) {
			if(numActiveA > a) {
				numActiveA--;
			} else {
				cout << time << "\n";
				break;
			}
		} 
		
		/*if(activeS[j] == -1) {
			numActiveA++;
			
		}
		activeS[j]=max(activeS[j],time);*/
		//lastTime = time;
	}
	
	/*int maximus = 0;
	for(int i=0;i<s;i++) {
		maximus=max(maximus,activeS[i]);
	}	*/
	
	/*int maxMin = 0;
		int bestA = -1;
		int bestS = -1;
		for(int i=0;i<a;i++) {
			if(activeA[i]) continue;
			while(activeS[distmaps[i][ps[i]].second]) ps[i]++;
			if(maxMin < distmaps[i][ps[i]].first) {
				maxMin = distmaps[i][ps[i]].first;
				bestS = distmaps[i][ps[i]].second;
				bestA = i;
			}
		}*//*
		maximus = max(maximus,maxMin);
		activeA[bestA] = true;
		activeS[bestS] = true;
		
		numActiveA++;*/
	
	// BFS to find vertex set S
	/*std::vector<int> vis(N, false); // visited flags
	std::queue<int> Q; // BFS queue (from std:: not boost::)
	vis[sss] = true; // Mark the source as visited
	for(boost::tie(e, eend) = boost::out_edges(boost::vertex(sss,G), G); e != eend; ++e) if(rescapacitymap[e] > 0) Q.push(target(*e, G));
	while (!Q.empty()) {
		const int u = Q.front();
		Q.pop();
		OutEdgeIt ebeg, eend;
		for (tie(ebeg, eend) = out_edges(u, G); ebeg != eend; ++ebeg) {
			const int v = target(*ebeg, G);
			// Only follow edges with spare capacity
			if (rescapacitymap[*ebeg] == 0 || ) continue;
			agents[v].push_back()
			if(v < n) {
				
			}
			
			vis[v] = true;
			Q.push(v);
		}
	}*/
	
	/*for(boost::tie(e, eend) = boost::out_edges(boost::vertex(sss,G), G); e != eend; ++e) {
		if(rescapacitymap[e] == 0) continue;
		
		const int v = target(*e, G);
		
	}
	for(int i=0;i<n;i++) {
		for(boost::tie(e, eend) = boost::out_edges(boost::vertex(sss,G), G); e != eend; ++e) {
			weightmap[*e] = 0;
			weightmap[revedgemap[*e]] = INT_MAX;
		}
	}*/
	
    // BFS to find vertex set S
	/*vector<int> vis(n+2, false); // visited flags
	vector<vector<int>> agents(n+2);
	vector<int> 
	std::queue<int> Q; // BFS queue (from std:: not boost::)
	vis[sss] = true; // Mark the source as visited
	
	
	
	OutEdgeIt ebeg, eend;
	for (tie(ebeg, eend) = out_edges(sss, G); ebeg != eend; ++ebeg) {
		const int v = target(*ebeg, G);
		agents[v].push_back(0);
		if(agents[v].size() == howMany[v]) Q.push(v);
	}
	
	for(int i=0;i<n;i++) {
		for (tie(ebeg, eend) = out_edges(sss, G); ebeg != eend; ++ebeg) {
			const int v = target(*ebeg, G);
			agents[v].push_back(0);
			if(agents[v].size() == howMany[v]) Q.push(v);
		}
	}*/
	
	/*while (!Q.empty()) {
		const int u = Q.front();
		Q.pop();
		OutEdgeIt ebeg, eend;
		for (tie(ebeg, eend) = out_edges(u, G); ebeg != eend; ++ebeg) {
			const int v = target(*ebeg, G);
			// Only follow edges with spare capacity
			if (rescapacitymap[*ebeg] == 0 || vis[v]) continue;
			agents[v].push_back()
			if(v < n) {
				
			}
			
			vis[v] = true;
			Q.push(v);
		}
	}*/

	
	//cout<<"\n";
    return 0;
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}

