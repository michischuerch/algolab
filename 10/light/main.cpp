// light

// idea: use triangulation for efficient neighborhood calculation
// use binary search for finding first light where everybody is hit by 
// some light. 

// main insight: binary search is useful when we have an ordered sequence


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <iostream>
#include <vector>
#include <map>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;


using namespace std;

bool checkAllHit(bool printAll, int p, long h, map<K::Point_2,int> &idMap, vector<K::Point_2> &lamps,vector<vector<int>> &participants) {
	int m = participants.size();
	// construct triangulation
	vector<K::Point_2> pts;
	pts.reserve(p);
	for(int i=0;i<p;i++) pts.push_back(lamps[i]);
	
	Triangulation t;
	t.insert(pts.begin(), pts.end());
	
	int counter=0;
	for(int j=0;j<m;j++) {
		int x = participants[j][0];
		int y = participants[j][1];
		int r = participants[j][2];
		K::Point_2 ppp(x,y); 
		K::Point_2 v = t.nearest_vertex(ppp)->point();
		if(CGAL::squared_distance(ppp,v) < (h+r)*(h+r)) {
			counter++;
		} else if(printAll) {
			cout << j << " ";
		}else return false;
	}
	if(counter==m)return true;
	else return false;
}

int testcases()
{
	int m,n; cin>>m>>n;
	vector<vector<int>> participants(m);
	for(int j=0;j<m;j++) {
		int x,y,r; cin>>x>>y>>r;
		vector<int> part = {x,y,r};
		participants[j] = part;
	}
	long h; cin>>h; // we know that h = r, bcs angle is 45 degree
	
	map<K::Point_2,int> idMap;
	vector<K::Point_2> lamps(n);
	for (int i = 0; i < n; ++i) {
		cin >> lamps[i];
		idMap[lamps[i]] = i;
	}
	
	if(checkAllHit(true,lamps.size(),h,idMap, lamps,participants)) {		
		int lmin = 0,lmax = n;
		// binary search
		while(lmin != lmax) {
			int p = (lmin + lmax)/2;
			
			if(checkAllHit(false,p,h,idMap, lamps,participants)) 
				lmax = p;
			else 
				lmin = p + 1;	
		}
		checkAllHit(true,lmax-1,h,idMap, lamps,participants);
	}
	
	cout << "\n";
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
