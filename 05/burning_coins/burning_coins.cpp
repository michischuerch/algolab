#include <iostream> 
#include <vector>
#include <unordered_map>

using namespace std;

int recursion(vector<int> &data, bool myTurn, int left, int right, vector<vector<int>> &memo) {
	if(myTurn && left==right) return data[left];
	else if(!myTurn && left==right) return 0;
	
	int takeLeft, takeRight;
	if(memo[left+1][right] == -1) {
		memo[left+1][right] = recursion(data,!myTurn,left+1,right, memo);
	} 
	takeLeft = memo[left+1][right];
	
	if(memo[left][right-1] == -1) {
		memo[left][right-1] = recursion(data,!myTurn,left,right-1,memo);
	} 
	takeRight = memo[left][right-1];
	
	
	
	if(myTurn) {
		if(takeLeft + data[left] > takeRight + data[right]) return takeLeft + data[left]; 
		else return takeRight + data[right];
	} else {
		if(takeLeft < takeRight) return takeLeft; 
		else return takeRight;
	}
}

// wrong
int easySolution(vector<int> &data, int left, int right, int sumLeft, int sumRight) {
	
	if(right-left+1 == 2) {
		return max(data[left],data[right]);
	}
	
	if(sumLeft > sumRight) {
		if(data[left+1] < data[right-1]) return easySolution(data,left+1,right-1,sumLeft-data[left],sumRight-data[right]);
		else return easySolution(data,left+2,right,sumLeft-data[left],sumRight-data[left+1]);
	} else {
		if(data[left] < data[right-2]) return easySolution(data,left,right-2,sumLeft-data[right-1],sumRight-data[right]);
		else return easySolution(data,left+1,right-1,sumLeft-data[left],sumRight-data[left]);
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	int t;
	cin >> t;
	while(t--) {
		// data
		int n;
		cin >> n;
		vector<int> data(n);
		for(int i=0;i<n;i++) cin >> data[i];
		
		//unordered_map<int, unordered_map<int,int> > memo;
		vector<vector<int> > memo(n);
		for(int i=0;i<n;i++) {
			vector<int> m(n,-1);
			memo[i] = m;
		}
		
		/*int allLeft = 0;
		for(int i=0;i<n;i+=2) allLeft += data[i];
		int allRight = 0;
		for(int i=1;i<n;i+=2) allRight += data[i];*/
		
		/*if(n%2 == 0) {
			cout << easySolution(data,0,n-1,allLeft,allRight) << "\n";
		} else {
			cout << "aha\n";
		}*/
		
		cout << recursion(data, true, 0, n-1, memo) << "\n";
		
		
	}
}
