// light pattern

#include <iostream>
#include <vector>
#include <utility>
using namespace std;

pair<int,int> recursion(int pos, vector<int> &data, int k) {
	pair<int,int> result;
	if(pos==0) result = make_pair(min(data[0],k-data[0]+1),min(data[0]+1,k-data[0]));
	else {
		pair<int,int> lastResult = recursion(pos-1, data, k);
		result = make_pair(min(lastResult.first + data[pos],lastResult.second + k-data[pos]+1),min(lastResult.first + data[pos]+1,lastResult.second + k-data[pos]));
	}
	cout << result.first << "/" << result.second << " ";
	return result;
}
int main() {
	int t; cin>>t;
	while(t--) {
		int n,k,x; cin>>n>>k>>x;
		vector<int> data(n/k);
		for(int i=0;i<n/k;i++) { 
			int countSame = 0;
			for(int j=0;j<k;j++) {
				bool next; cin>>next;
				bool shouldBe = x & (1 << k-(j%k +1));
				if(next==shouldBe) countSame++;
			}
			data[i]=countSame;
		}
		pair<int,int> s = make_pair(0,0);
		for(int i=0;i<n/k;i++) s=make_pair(min(s.first + data[i],s.second + k-data[i]+1),min(s.first + data[i]+1,s.second + k-data[i]));
		cout << min(s.first+1,s.second) << "\n";
	}
	return 0;
}
