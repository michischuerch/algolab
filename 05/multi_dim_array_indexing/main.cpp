// notebook and the queries

#include <iostream>
#include <vector>
#include <climits>
using namespace std;

long getPosition(vector<int> hpBounds, vector<int> positions) {
	long p=0;
	int k=positions.size();
	for(int i=0;i<k;i++) {
		long current = positions[i];
		for(int j=0;j<i;j++) current*= hpBounds[j];
		p+= current;
	}
	return p;
}

void testcases() {
	int k,n,q; cin>>k>>n>>q;
	
	vector<int> hpBounds(k);
	vector<int> hpLeft(k);
	for(int i=0;i<k;i++) {
		int a,b; cin>>a>>b;
		hpBounds[i]=b-a+1;
		hpLeft[i]=a;
	}
	long bigNumba=1;
	for(int i=0;i<k;i++) bigNumba*=hpBounds[i];
	vector<int> experiments(bigNumba,-1);
	
	for(int i=0;i<n;i++) {
		vector<int> params(k);
		for(int j=0;j<k;j++) {cin >> params[j]; params[j]-=hpLeft[j]; }
		int r; cin >> r;
		experiments[getPosition(hpBounds,params)] = r;
	}
	for(int i=0;i<q;i++) {
		vector<int> params(k);
		for(int j=0;j<k;j++) {cin >> params[j]; params[j]-=hpLeft[j]; }
		cout << experiments[getPosition(hpBounds,params)] << "\n";
	}
}

int main () {	
	int t;cin >> t;
	while(t--) testcases();
	return 0;
}
