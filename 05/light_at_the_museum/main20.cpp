//latm


#include <iostream>
#include <vector>
#include <utility>

using namespace std;

int main() {
	int t; cin >> t;
	while(t--) {
		int N,M; cin >> N >> M;
		
		vector<int> desired(M);
		for(int i=0;i<M;i++) cin >> desired[i];
		
		vector<pair<vector<int>,vector<int>>> data(N); // room -> (list on, list off)
		for(int i=0;i<N;i++) {
			vector<int> on(M);
			vector<int> off(M);
			for(int j=0;j<M;j++) {
				cin >> on[j];
				cin >> off[j];
			}
			data[i] = make_pair(on,off);
		}
		
		int bestSolution = -1;
		for(int s=0;s < 1 << N;s++) {
			for(int j=0;j<M;j++) {
				int sum = 0;
				int numSwitches = 0;
				for(int i=0;i<N;i++) {
					if(s & 1<<i) sum+=data[i].first[j];
					else {
						sum+=data[i].second[j];
						numSwitches++;
					}
				}
				if(sum!=desired[j]) break;
				if(j==M-1) bestSolution == -1 ? bestSolution = numSwitches : bestSolution = min(bestSolution,numSwitches);
			}
		}
		bestSolution == -1 ? cout << "impossible\n" : cout << bestSolution << "\n";
	}
}
