//latm

// idea: 1. transform lightbulbs into number (convenience)
// 2. split all subset sums into 2 subsets 
// 3. sort second subset and use binary search of each element in subset 1 to find matching partner in subset 2.

// i learnt: - cgal can be quite useful to transform something into a number of arbitrary size
// - i can use lambda expressions and change the comparison function in binary search

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <vector>
#include <utility>
#include <set>
#include <algorithm>

typedef CGAL::Exact_predicates_exact_constructions_kernel K;

using namespace std;

// this was done using the template on the c++ manual, section algorithm -> binary search
template<class ForwardIt, class T, class Compare>
int my_binary_search(ForwardIt first, ForwardIt last, const T& value, Compare comp)
{
    first = std::lower_bound(first, last, value, comp);
    if(!(first == last) && !(comp(value, *first))) return (*first).second;
    else return -1;
}
// idea is to use the sums calculated before to be as efficient as possible
vector<pair<K::FT,int>> calcSubsets(int start, int finish, vector<pair<K::FT,K::FT>> data) {
	vector<pair<K::FT,int>> result;
	for(int i=start;i<finish;i++) {
		if(i==start) {
			result.push_back(make_pair(data[i].first,0));
			result.push_back(make_pair(data[i].second,1));
		} else {
			int currentSize=result.size();
			for(int j=0;j<currentSize;j++) {
				result.push_back(make_pair(result[j].first+data[i].second,result[j].second+1));
				result[j].first+=data[i].first;
			}
		}
	}
	return result;
}


int main() {
	int t; cin >> t;
	while(t--) {
		int N,M; cin >> N >> M;
		
		K::FT desired(0);
		for(int j=0;j<M;j++) {
			int next; cin >> next;
			K::FT nextFT(next);
			for(int i=0;i<j;i++) nextFT*=101*N;
			desired+= nextFT;
		}
		// setup
		// read in data and transform into numbers 
		vector<pair<K::FT,K::FT>> data;
		
		for(int i=0;i<N;i++) {
			K::FT newOn(0);
			K::FT newOff(0);
			for(int j=0;j<M;j++) {
				int on,off; cin>>on>>off;
				K::FT onFT(on); K::FT offFT(off);
				for(int i=0;i<j;i++) { onFT*=101*N; offFT*=101*N; }
				newOn+=onFT; newOff+=offFT;
			}
			data.push_back(make_pair(newOn,newOff));
		}
		
		// split data into two sets of all possible subset sums
		vector<pair<K::FT,int>> firstHalf = calcSubsets(0,N/2,data); 
		vector<pair<K::FT,int>> secondHalf = calcSubsets(N/2,N,data); 
		
		sort(secondHalf.begin(),secondHalf.end());
		
		// for each element in subset 1 look with binary search in subset 2 and if they match save best solution
		int bestSolution = -1;
		for(int i=0;i<firstHalf.size();i++) {
			pair<K::FT,int> searching = firstHalf[i];
			searching.first=desired-searching.first;
			int result = my_binary_search(secondHalf.begin(),secondHalf.end(),searching,[] (const pair<K::FT,int>& lhs, const pair<K::FT,int>& rhs) {return lhs.first < rhs.first;});
			if(result!=-1) bestSolution == -1 ? bestSolution=result+firstHalf[i].second : bestSolution=min(bestSolution,result+firstHalf[i].second);
		}
		bestSolution == -1 ? cout << "impossible\n" : cout << bestSolution << "\n";
	}
}
