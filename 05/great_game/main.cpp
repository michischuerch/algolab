// great game

// idea: use recursion and play min/max game
// -> use memoization 
// sherlock wins if he has less turns or equal turns and he played an odd amount of turns

#include <iostream>
#include <vector>
using namespace std;

int recursion(int currentNode, vector<vector<int>> &nextList, bool iWantFast, vector<int> &memoFast, vector<int> &memoSlow) {
	int n=nextList.size();
	int solution = -1;
	if(currentNode == n-1) solution = 0;
	else {
		for(int i=0;i<nextList[currentNode].size();i++) {
			int nextSolution;
			
			if(iWantFast) {
				memoSlow[nextList[currentNode][i]] == -1 ? nextSolution = recursion(nextList[currentNode][i], nextList, !iWantFast, memoFast,memoSlow) : nextSolution = memoSlow[nextList[currentNode][i]];
				solution == -1 ? solution = nextSolution + 1 : solution = min(nextSolution + 1,solution);
			}
			else {
				memoFast[nextList[currentNode][i]] == -1 ? nextSolution = recursion(nextList[currentNode][i], nextList, !iWantFast, memoFast,memoSlow) : nextSolution = memoFast[nextList[currentNode][i]];
				solution == -1 ? solution = nextSolution + 1: solution = max(nextSolution + 1,solution);
			}
		}
	}
	if(iWantFast) memoFast[currentNode] = solution;
	else memoSlow[currentNode] = solution;
	return solution;
}

void testcases() {
	int n,m,r,b;
	cin >> n >> m >> r >> b;
	
	vector<vector<int> > nextList(n);
	for(int i=0;i<n;i++) { vector<int> list; nextList[i] = list; }
	
	for (int i=0; i<m; ++i) {
		int u,v; cin>>u>>v;
		nextList[u-1].push_back(v-1);
	}
	
	vector<int> memoFast(n,-1);
	vector<int> memoSlow(n,-1);
	int solutionSherlock = recursion(r-1,nextList,true,memoFast,memoSlow);
	int solutionMoriarty = recursion(b-1,nextList,true,memoFast,memoSlow);
	
	if(solutionSherlock < solutionMoriarty || (solutionSherlock == solutionMoriarty && solutionSherlock % 2 == 1) ) cout << "0\n";
	else cout << "1\n";
	
}
int main() {
	ios_base::sync_with_stdio(false);
	int T;	cin >> T;
	while(T--)	testcases();
	return 0;
}
