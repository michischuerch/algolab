// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>
#include <utility>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
// Namespaces
using namespace std;
using namespace boost;


// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef adjacency_list<vecS, vecS, directedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		no_property,				// interior properties of vertices	
		property<edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef property_map<Graph, edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t

int main() {
	
	int V,E,q;
	cin >> V >> E >> q;
	
	vector<pair<int,int> > v;
	for(int i=0;i<V;i++) {
		int a,b;
		cin >> a >> b;
		v.push_back(make_pair(a,b));
	}
	
	Graph G(V);	// Creates an empty graph on V vertices
	WeightMap weightmap = get(edge_weight, G);
	for (int i = 0; i < E; ++i) {
		int u, v, w;		// Each edge: <from> <to> <weight>
		cin >> u >> v >> w;
		Edge e;	bool success;			
		tie(e, success) = add_edge(u, v, G);	
		weightmap[e] = w;
	}
	for(int i=0;i<q;i++) {
		int s,t;
		cin >> s >> t;
		if(s==t) {cout << "0\n"; continue; }
		// Dijkstra shortest paths
		// =======================
		vector<Vertex> predmap(V);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
		vector<long> distmap(V);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
		Vertex start = s;
		dijkstra_shortest_paths(G, start, // We MUST provide at least one of the two maps
			predecessor_map(make_iterator_property_map(predmap.begin(), get(vertex_index, G))).	// predecessor map as Named Parameter
			distance_map(make_iterator_property_map(distmap.begin(), get(vertex_index, G))));	// distance map as Named Parameter
		predmap[t] != t ? cout << distmap[t] << "\n" : cout << "unreachable\n";
	}
	return 0;
}
