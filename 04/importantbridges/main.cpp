// important bridges

// idea use "biconneted component" from BGL and count the number of edges in the components, if 1 then it is a critical edge
// -> most difficult was to properly define the edge_component type ... we have to also alter the "Graph" typedef

#include <iostream>
#include <vector>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/biconnected_components.hpp>

// I got this from the sample in biconnected components
namespace boost
{
  struct edge_component_t
  {
    enum
    { num = 555 };
    typedef edge_property_tag kind;
  }
  edge_component;
}

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		no_property,				// interior properties of vertices	
		property<edge_component_t, int> 		// interior properties of edges
		>					Graph;
typedef graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef graph_traits<Graph>::edge_descriptor		Edge;

void testcases() {
	int n,m; cin>>n>>m;
	Graph G(n);	
	
	for(int i=0;i<m;i++) {
		int e1,e2;cin>>e1>>e2;
		Edge e;
		tie(e, ignore) = add_edge(e1, e2, G);
	}
	
	property_map<Graph, edge_component_t>::type component = get(edge_component, G);
	int size = biconnected_components(G,component);
	
	vector<int> countComponents(size);
	vector<pair<int,int> > solution;
	
	EdgeIt ei, ei_end;
	for(boost::tie(ei, ei_end) = edges(G); ei != ei_end; ++ei) countComponents[component[*ei]]++;
	for(boost::tie(ei, ei_end) = edges(G); ei != ei_end; ++ei) 
		if(countComponents[component[*ei]] == 1) 
			if(source(*ei, G) < target(*ei,G)) solution.push_back(make_pair(source(*ei, G),target(*ei,G)));
			else solution.push_back(make_pair(target(*ei, G),source(*ei,G)));
	
	sort(solution.begin(),solution.end());
	
	cout << solution.size() << "\n";
	for(int i=0;i<solution.size();i++) cout << solution[i].first << " " << solution[i].second << "\n";
}

int main() {
	ios_base::sync_with_stdio(false); 
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
