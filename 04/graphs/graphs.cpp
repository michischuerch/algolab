// ALGOLAB BGL Tutorial 1
// Tutorial example problem

// Compile and run with one of the following:
// g++ -std=c++11 -O2 bgl-tutorial_problem.cpp -o bgl-tutorial_problem; ./bgl-tutorial_problem < bgl-tutorial_problem.in
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 bgl-tutorial_problem.cpp -o bgl-tutorial_problem; ./bgl-tutorial_problem < bgl-tutorial_problem.in

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/bellman_ford_shortest_paths.hpp>

// Namespaces
using namespace std;
using namespace boost;

// Directed graph with integer weights on edges.
typedef adjacency_list<vecS, vecS, directedS,
		no_property,
		property<edge_weight_t, int>
		>					Graph;
typedef graph_traits<Graph>::vertex_descriptor		Vertex;	// Vertex type		
typedef graph_traits<Graph>::edge_descriptor		Edge;	// Edge type
typedef graph_traits<Graph>::edge_iterator		EdgeIt;	// Edge iterator
// Property map edge -> weight
typedef property_map<Graph, edge_weight_t>::type	WeightMap;
//typedef typename property_traits<DistanceMap>::value_type value_type;


// Functions
// ========= 
void testcases() {
	// Read and build graph
	int V, E;	// 1st line: <vertex_no> <edge_no> <target>
	cin >> V >> E;
	Graph G(V);	// Creates an empty graph on V vertices
	WeightMap weightmap = get(edge_weight, G);
	for (int i = 0; i < E; ++i) {
		int u, v, w;		// Each edge: <from> <to> <weight>
		cin >> u >> v >> w;
		Edge e,e2;	bool success,success2;			
		tie(e, success) = add_edge(u, v, G);
		tie(e2, success2) = add_edge(v, u, G);	
		weightmap[e] = w;
		weightmap[e2] = w;
	}

	// Kruskal minimum spanning tree
	// =============================
	vector<Edge>	mst; // We must use this vector to store the MST edges (not as a property map!)
	// We can use the following vectors as Exterior Property Maps if we want to access additional information computed by Union-Find:	
	vector<Vertex>	kruskalpredmap(V);	// Stores predecessors needed for Union-Find (NOT the MST!)
	vector<int>	rankmap(V);		// Stores ranks needed for Union-Find
	kruskal_minimum_spanning_tree(G, back_inserter(mst),	// kruskal_minimum_spanning_tree(G, back_inserter(mst)); would be fine as well 
			rank_map(make_iterator_property_map(rankmap.begin(), get(vertex_index, G))).
			predecessor_map(make_iterator_property_map(kruskalpredmap.begin(), get(vertex_index, G))));			
	int totalweight = 0;
	// go through the minimum spanning tree with an iterator
	vector<Edge>::iterator	mstbeg, mstend = mst.end();
	for (mstbeg = mst.begin(); mstbeg != mstend; ++mstbeg) {
		totalweight += weightmap[*mstbeg];
	}
	cout << totalweight << " ";
	// EdgeIterators
	// =============
	EdgeIt ebeg, eend;
	/*for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg) {	// edges(G) returns a pair of iterators which define a range of all edges. 
		weightmap[*ebeg] = -weightmap[*ebeg];
	}*/
	
	
	vector<Vertex> predmap(V);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	vector<int> distmap(V);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
	Vertex start = 0;
	dijkstra_shortest_paths(G, start, // We MUST provide at least one of the two maps
		predecessor_map(make_iterator_property_map(predmap.begin(), get(vertex_index, G))).	// predecessor map as Named Parameter
		distance_map(make_iterator_property_map(distmap.begin(), get(vertex_index, G))));	
	int maxdist = 0;
	for (int i = 0; i < V; ++i) {
		if (distmap[i] < INT_MAX && distmap[i] > maxdist) maxdist = distmap[i];
	}
	
	cout << maxdist << "\n";
	
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false);
	int t;	cin >> t;	// First input line: Number of testcases.
	while(t--)	testcases();
	return 0;
}
