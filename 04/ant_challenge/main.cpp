// ant challange

// idea: 1. build networks 2. use dijkstra
// coolio: - map can handle pair as keys (is not a reference)
// uufpasse: priority queue is ordered with biggest element on front. Need to override with std::greater
// uufpasse2: top() denn sofort pop() für priority_queue! ha fuck up gha wege dem



// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>
#include <queue>
#include <utility>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
// Namespaces
using namespace std;
using namespace boost;

// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef adjacency_list<vecS, vecS, undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		no_property,				// interior properties of vertices	
		property<edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef property_map<Graph, edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t

// Functions
// ========= 
void testcases() {
	int n,e,s,a,b; cin>>n>>e>>s>>a>>b;
	
	vector<vector<int>> edges(n); // tree1 -> list of trees connected to tree1 
	for(int i=0;i<n;i++) {vector<int> empty; edges[i]=empty;}
	map<pair<int,int>,vector<int> > edgeWeights; // t1,t2 -> weights
	
	for(int i=0;i<e;i++) {
		int t1,t2; cin>>t1>>t2;
		vector<int> ew(s); 
		for(int ss=0;ss<s;ss++) cin >> ew[ss];
		
		edges[t1].push_back(t2);
		edges[t2].push_back(t1);
		edgeWeights[make_pair(t1,t2)] = ew;
		edgeWeights[make_pair(t2,t1)] = ew;
	}
	
	Graph G(n);	
	WeightMap weightmap;
	
	// build hive net...
	for(int i=0;i<s;i++) {
		int h; cin>>h;
		
		vector<bool> visited(n,false);
		priority_queue<pair<int,pair<int,int>>, vector<pair<int,pair<int,int>>>, std::greater<pair<int,pair<int,int>>>> edgesToLookAt; // (distance from current network, (fromtree, totree))
		// fill in hivetree...
		visited[h] = true;
		for(int j=0;j<edges[h].size();j++) {
			pair<int,int> e = make_pair(h,edges[h][j]);
			edgesToLookAt.push(make_pair(edgeWeights[e][i],e));
		}
		//build network
		while(!edgesToLookAt.empty()) {
			pair<int,pair<int,int>> next = edgesToLookAt.top();
			edgesToLookAt.pop();
			int w = next.first;
			int from = next.second.first;
			int to = next.second.second;
			if(!visited[to]) {
				visited[to] = true;
				for(int j=0;j<edges[to].size();j++) {
					pair<int,int> e = make_pair(to,edges[to][j]);
					edgesToLookAt.push(make_pair(edgeWeights[e][i],e));
				}
				// we got a new edge for our real graph!
				// we could be more efficient by only adding if it is the smallest way but it works anyways...
				Edge e;
				tie(e, ignore) = add_edge(from, to, G);
				weightmap[e] = w;
			}
		}
	}
	
	// Dijkstra shortest paths
	// =======================
	vector<Vertex> predmap(n);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	vector<int> distmap(n);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
	dijkstra_shortest_paths(G, a, // We MUST provide at least one of the two maps
		predecessor_map(make_iterator_property_map(predmap.begin(), get(vertex_index, G))).	// predecessor map as Named Parameter
		distance_map(make_iterator_property_map(distmap.begin(), get(vertex_index, G))));	// distance map as Named Parameter
	cout << distmap[b] << "\n";
}
int main() {
	ios_base::sync_with_stdio(false); 
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
