// buddy selection

// 1. save student as set, 2. use set_intersection->if the size is bigger than f add edge, 3. edmonds cardinality matching
// somehow it worked right away. Sets are sorted therefore set_intersection works out of the box

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
// Namespaces
using namespace std;
using namespace boost;


// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef adjacency_list<vecS, vecS, undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		no_property,				// interior properties of vertices	
		property<edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	

void testcases() {
	int n,c,f;cin>>n>>c>>f;
	vector<set<string>> students(n);
	for(int i=0;i<n;i++)
		for(int j=0;j<c;j++) {
			string next; cin>>next;
			students[i].insert(next);
		}
	
	// build graph
	Graph G(n);
	for(int i=0;i<n;i++)
		for(int j=i+1;j<n;j++) {
			// set intereŝection can be found in algorithms tab in the judge
			std::vector<string> v_intersection;
			set_intersection(students[i].begin(),students[i].end(),
								students[j].begin(),students[j].end(),
								std::back_inserter(v_intersection));
			if(v_intersection.size() > f) {
				Edge e;
				tie(e, ignore) = add_edge(i, j, G);
			}
		}
	
	// use edmonds maximum matching cardinality algo...
	// Edmonds' maximum cardinality matching
	// =====================================
	vector<Vertex> matemap(n);		// We MUST use this vector as an Exterior Property Map: Vertex -> Mate in the matching
	edmonds_maximum_cardinality_matching(G, make_iterator_property_map(matemap.begin(), get(vertex_index, G)));
	int matchingsize = matching_size(G, make_iterator_property_map(matemap.begin(), get(vertex_index, G)));
	matchingsize*2 == n ? cout << "not optimal\n" : cout << "optimal\n";
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
