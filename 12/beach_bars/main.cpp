// beach bars

// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>

using namespace std;

int testcases() {
	int n; cin>>n;
	
	vector<int> xs(n);
	for(int i=0;i<n;i++) cin>>xs[i];
	
	sort(xs.begin(),xs.end());
	
	int left = 0;
	int right = 0;
	
	
	int maxBars = 0;
	int minDistance = INT_MAX;
	vector<int> optimalLocations;
	while(right<xs.size()-1) {
	
		if(xs[right+1]-xs[left] <= 200) {
			right++;
		} else {
			left++;
		}
		//cout << xs[left] << " " << xs[right] << " ";
		
		// calc distance
		int d = (xs[right]-xs[left]+1)/2;
		
		int numBars=right-left+1;
		if(maxBars<numBars) {
			maxBars=numBars;
			minDistance=d;
			
			optimalLocations.clear();
			if((xs[right]-xs[left])%2==1) {
				optimalLocations.push_back(xs[left]+d-1);
			}
			optimalLocations.push_back(xs[left]+d);
			
			//cout << "(" << xs[left] << " " << xs[right] << " " << minDistance << ") ";
		} else if(maxBars==numBars) {
			if(minDistance>d) {
				minDistance=d;
				optimalLocations.clear();
				if((xs[right]-xs[left])%2==1) {
					optimalLocations.push_back(xs[left]+d-1);
				}
				optimalLocations.push_back(xs[left]+d);
			} else if(minDistance==d) {
				if((xs[right]-xs[left])%2==1) {
					optimalLocations.push_back(xs[left]+d-1);
				}
				optimalLocations.push_back(xs[left]+d);
				
			}
		}
	}
	
	cout << maxBars << " " << minDistance << "\n";
	for(int i=0;i<optimalLocations.size();i++) cout << optimalLocations[i] << " ";
	cout << "\n";
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
