#include <vector>
#include <map>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Vertex_iterator  Vertex_iterator;

using namespace std;

int testcases()
{
	// read number of points
	int n,m,p; cin>>n>>m>>p;
	// read points
	std::vector<K::Point_2> pts;
	pts.reserve(n);
	for (std::size_t i = 0; i < n; ++i) {
		K::Point_2 p;
		std::cin >> p;
		pts.push_back(p);
	}
	// construct triangulation
	Triangulation t;
	t.insert(pts.begin(), pts.end());
	
	map<Triangulation::Vertex_handle,int> pointMap;
	int i=0;
	for (Vertex_iterator v = t.finite_vertices_begin(); v != t.finite_vertices_end(); ++v) {
		pointMap[v] = i;
		i++;
	}
	vector<int> ss; 
	vector<int> tt;
	
	for(int i=0;i<m;i++) {
		K::Point_2 s_,t_;
		cin>>s_>>t_;
		ss.push_back(pointMap[t.nearest_vertex(s_)]);
		tt.push_back(pointMap[t.nearest_vertex(t_)]);
	}
	
	
	
	
	cout<<"lul\n";
	//for (Edge_iterator e = t.finite_edges_begin(); e != t.finite_edges_end(); ++e) std::cout << t.segment(e) << "\n";
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}

