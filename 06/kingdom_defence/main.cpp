// kingdom defence

// idea: use supply/demand with min flow already flowing
// where we look at a network with the minimal flows pushed through (t=1)
// this means a demand is when a node still needs more members in the castle after min flow is set
// and supply is when node has still some to give
// the capacity of a edge is also reduced by its min flow


// ALGOLAB BGL Tutorial 2
// Flow example demonstrating
// - interior graph properties for flow algorithms
// - custom edge adder

// Compile and run with one of the following:
// g++ -std=c++11 -O2 flows.cpp -o flows ./flows
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 flows.cpp -o flows; ./flows

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>
// Namespaces
using namespace std;
using namespace boost;


// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	property_map<Graph, edge_capacity_t>::type		EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type		ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	graph_traits<Graph>::edge_descriptor			Edge;


// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, reverseE;
		bool success;
		tie(e, success) = add_edge(from, to, G);
		tie(reverseE, success) = add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[reverseE] = 0;
		revedgemap[e] = reverseE;
		revedgemap[reverseE] = e;
	}
};


// Functions
// =========
// Function for an individual testcase
void testcases() {
	
	long l,p; cin >> l >> p;
	
	// Create Graph and Maps
	Graph G(l);
	EdgeCapacityMap capacitymap = get(edge_capacity, G);
	ReverseEdgeMap revedgemap = get(edge_reverse, G);
	ResidualCapacityMap rescapacitymap = get(edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);

	Vertex src = add_vertex(G);
	Vertex sink = add_vertex(G);

	long wantedFlow = 0;
	vector<int> supplies(l);

	// Add edges
	for(int i=0;i<l;i++) {
		long g,d; cin >> g >> d;
		supplies[i] = g - d;
	}

	long sumMinCapacity = 0;

	for(int i=0;i<p;i++) {
		long f,t,c,C; cin >> f >> t >> c >> C;
		eaG.addEdge(f,t,C-c);
		supplies[f] -= c;
		supplies[t] += c;
	}
	
	for(int i=0;i<l;i++) {
		if(supplies[i] > 0) eaG.addEdge(src,i,supplies[i]);
		else if(supplies[i] < 0) {
			eaG.addEdge(i,sink,-supplies[i]);
			wantedFlow -= supplies[i];
		}
	}
	
	// Calculate flow
	// If not called otherwise, the flow algorithm uses the interior properties
	// - edge_capacity, edge_reverse (read access),
	// - edge_residual_capacity (read and write access).
	long flow = push_relabel_max_flow(G, src, sink);
	flow == wantedFlow ? cout << "yes\n" : cout << "no\n";
}

// Main function to loop over the testcases
int main() {
	ios_base::sync_with_stdio(false);
	int T;	cin >> T;
	while(T--)	testcases();
	return 0;
}
