// clues

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>
#include <map>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
#include <boost/graph/breadth_first_search.hpp>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Finite_faces_iterator  Face_iterator;

// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t

using namespace boost;using namespace std;

void extendGraph(int n, Graph &G, long r_squared,vector<K::Point_2> &pts) {
	for(int src=0;src<n;src++) {
		OutEdgeIt ebeg, eend;
		for (boost::tie(ebeg, eend) = boost::out_edges(src, G); ebeg != eend; ++ebeg) {
			const int u = boost::target(*ebeg, G);
			OutEdgeIt ebeg2, eend2;
			vector<int> add_edges;
			for (boost::tie(ebeg2, eend2) = boost::out_edges(u, G); ebeg2 != eend2; ++ebeg2) {
				const int v = boost::target(*ebeg2, G);
				if(v == src) continue;
				else if(CGAL::squared_distance(pts[v],pts[src])<=r_squared) {
					add_edges.push_back(v);
				}
			}
			for(int adde : add_edges) {
				Edge ee;
				//std::tie(std::ignore, flag) = edge(src,target,G)
				if(!edge(src, adde, G).second) {
					boost::tie(ee, ignore) = boost::add_edge(src, adde, G);
				}
			}
		}
	}
}


bool networkPossible(int n, Graph &G) {
	// BFS to find vertex set S
	std::vector<int> vis(n, -1); // visited flags
	std::queue<int> Q; // BFS queue (from std:: not boost::)
	int num = 0;
	for(int src=0;src<n;src++) {
		if(vis[src] != -1) continue;
		num++;
		vis[src] = 0; // color source with 0
		Q.push(src);
		while (!Q.empty()) {
			const int u = Q.front();
			Q.pop();
			OutEdgeIt ebeg, eend;
			for (boost::tie(ebeg, eend) = boost::out_edges(u, G); ebeg != eend; ++ebeg) {
				const int v = boost::target(*ebeg, G);
				// Only follow edges with spare capacity
				if (vis[u] == vis[v]) return false;
				if (vis[v] != -1) continue;
				vis[v] = 1-vis[u];
				Q.push(v);
			}
		}
	}
	return true;
}

vector<K::Point_2> getFinitePointsOfFaceHandle(K::Point_2 &p, Triangulation &t) {
	vector<K::Point_2> result;
	result.push_back(t.nearest_vertex(p)->point());
	return result;
}

bool findSameComponent(K::Point_2 &ai, K::Point_2 &bi, Triangulation &t, vector<int> &cm, map<K::Point_2,int> &idMap,long r_squared) {
	for(auto p1 : getFinitePointsOfFaceHandle(ai,t)) {
		if(CGAL::squared_distance(ai,p1)>r_squared) continue;
		
		for(auto p2 : getFinitePointsOfFaceHandle(bi,t)) {
			if(CGAL::squared_distance(bi,p2)>r_squared) continue;
			
			if(cm[idMap[p1]] == cm[idMap[p2]]) return true;
		}
	}
	
	return false;
}


void testcases() {
	long n,m,r; cin>>n>>m>>r;
	long r_squared = r*r; ///////////////// UUUUUUUUUFFFFFFFFFPAAAAAAAAASSSSEEEE ////
	
	vector<K::Point_2> pts;
	
	map<K::Point_2,int> idMap;
	
	for(int i=0;i<n;i++) {
		int x,y; cin>>x>>y;
		K::Point_2 p(x,y);
		idMap[p] = i;
		pts.push_back(p);
	}
	
	// construct triangulation
	Triangulation t;
	t.insert(pts.begin(), pts.end());
	
	bool np = true; // network possible
	Graph G(n);	
	
	for (Edge_iterator e = t.finite_edges_begin(); e != t.finite_edges_end(); ++e) {
		auto v0 = (e->first->vertex((e->second + 1) % 3))->point();
		auto v1 = (e->first->vertex((e->second + 2) % 3))->point();
		bool b = CGAL::squared_distance(v0,v1)<=r_squared;		
		if(b) {
			Edge ee;	
			boost::tie(ee, ignore) = boost::add_edge(idMap[v0], idMap[v1], G);	// Adds edge from u to v. If parallel edges are allowed, success is always true.
		}
	}
	
	std::vector<int> cm(n);	
	int ncc = boost::connected_components(G, boost::make_iterator_property_map(cm.begin(), boost::get(boost::vertex_index, G))); 
	
	extendGraph(n, G, r_squared, pts);
	
	// check 2. condition, no cycles that are even
	np = networkPossible(n,G);
	for(int j=0;j<m;j++) {
		K::Point_2 ai, bi;
		cin>>ai>>bi; 
		
		if(!np) {
			cout << "n";
		}
		else {
			// network exists, find path
			if(CGAL::squared_distance(ai,bi)<=r_squared) {
				cout << "y";
				continue;
			}
			
			if(findSameComponent(ai,bi,t,cm,idMap,r_squared)) cout << "y";
			else cout << "n";
			
			
		}
	}
	
	cout << "\n";
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
