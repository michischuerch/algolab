// le corbusier

// idea: use split / list 
// special: we can calc modulo in subsets and only save single values
// special2: we cannot use binary search and have to compare all elements in s1 with s2

// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>
#include <unordered_set>
#include <map>
using namespace std;

vector<int> allSubsets (vector<int> values, int modulo) {
	vector<int> result;
	map<int,bool> vis;
	for(int v : values) {
		int vmod = v % modulo;
		
		int n=result.size();
		for(int i=0;i<n;i++) {
			int next = (result[i]+vmod) % modulo;
			if(!vis[next]) {
				result.push_back(next);
				vis[next] = true;
			}
			
		}
		result.push_back(vmod);
	}
	return result;
}

bool search_solution(vector<int> s1, vector<int> s2, int i, int k) {
	for(int s : s1) {
		if(binary_search(s2.begin(),s2.end(),i+k-s)) return true;
		/*for(int ss : s2) {
			if((s+ss)%k == i) return true;
		}*/
	}
	return false;
}

int testcases() {
	int n,i,k; cin>>n>>i>>k;
	
	vector<int> h1(n/2);
	vector<int> h2(n-n/2);
	for(int ii=0;ii<n/2;ii++) cin>>h1[ii];
	for(int ii=0;ii<n-n/2;ii++) cin>>h2[ii];
	
	vector<int> s1 = allSubsets(h1,k);
	vector<int> s2 = allSubsets(h2,k);
	
	sort(s2.begin(), s2.end());
	
	if(search_solution(s1,s2, i, k)) cout << "yes";
	else cout << "no";
	
	
	cout << "\n";
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
