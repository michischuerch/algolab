// planks

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <cassert>

using namespace std;

vector<vector<long>> getSomeSides(int amount, vector<long> &data, long totalLength) {
	vector<vector<long>> allSides;
	if(amount==0) return allSides;
	int n = data.size();
	long k = totalLength/4;
	//cout << k << " ";
	
	for ( int s = 0; s < 1 << n ; ++s ){ // iterate through all subsets
		long sum = 0;
		for ( int i = 0; i < n ; ++i ){
			if( s & 1 << i ) sum += data[i]; // if i-th element in subset
		}
		//cout << sum << " ";
		if( sum == k ) {
			vector<long> newData;
			vector<long> side;
			for ( int i = 0; i < n ; ++i ){
				if( s & 1 << i ) {
					side.push_back(data[i]);
				} else {
					newData.push_back(data[i]);
				}
			}
			vector<vector<long>> allOtherSides = getSomeSides(amount-1, newData, totalLength);
			if(allOtherSides.size() == amount-1) {
				allSides=allOtherSides;
				allSides.push_back(side);
				return allSides;
			}
		}
	}
	return allSides;
}

vector<long> calcSubsetSums(vector<long> &data) {
	vector<long> result;
	int n=data.size();
	for ( int s = 0; s < 1 << n ; ++s ){ // iterate through all subsets
		long sum = 0;
		for ( int i = 0; i < n ; ++i ){
			if( s & 1 << i ) sum += data[i]; // if i-th element in subset
		}
		result.push_back(sum);
	}
	return result;
}

void testcases() {
	int n; cin>>n;
	vector<long> data(n);
	long totalLength=0;
	for(int i=0;i<n;i++) {
		cin>>data[i];
		totalLength+=data[i];
	}
	if(n<4 || totalLength%4!=0) {
		cout << 0 << "\n";
		return;
	}
	
	vector<vector<long>> r = getSomeSides(4,data,totalLength);
		
	if(r.size()==0) cout << 0;
	else {
		vector<vector<long>> ss;
		for(vector<long> r_i : r) {
			ss.push_back(calcSubsetSums(r_i));
		}
		
		/*int amount=1;
		for(int i=0;i<4;i++) {
			for(int j=i+1;j<4;j++) {
				for(int k=0;k<ss[i].size();k++) {
					for(int kk=0;kk<ss[j].size();kk++) {
						if(ss[i][k] == ss[j][kk]) amount++;
					}
				}
			}
		}*/
		
		
		
		
		cout << "1";
		
		
		
		
		
	}
	
	cout << "\n";
}

// Main function looping over the testcases
int main() {
	std::ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;	cin>>T;
	while(T--)	testcases();
	return 0;
}
