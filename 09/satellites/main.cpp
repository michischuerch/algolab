// satellites

// TIL: super hard to combine max flow with dijkstra.. do two graphs maybe?
// for DFS go to last sample of their code and copy the part below max flow (BGL is too complicated in my opinion)
// behinderts output format, wie immer uufpasse ...
// rescapacitymap[*ebeg] == 0 tells us that there is no flow on the edge

// useful slides: advanced flows 19 / 20
// note -> we dont have to do step 2 actually explicitly 

#include <iostream>
#include <cstdlib>
#include <vector>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>
#include <queue>
// Namespaces
using namespace boost;
using namespace std;

// BGL Graph definitions
// ===================== 
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > Graph;
// Interior Property Maps
typedef property_map<Graph, edge_capacity_t>::type      EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResidualCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type       ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor          Vertex;
typedef graph_traits<Graph>::edge_descriptor            Edge;
typedef graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator
typedef property_map<Graph, edge_weight_t>::type WeightMap;

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
	}
};

int main() {
	int t; cin>>t;
	
	while(t--) {
		int g,s,l; cin>>g>>s>>l;
		
		int sss = g+s;
		int ttt = g+s+1;
		
		
		// Create Graph and Maps
		Graph G(g+s+2);
		EdgeCapacityMap capacitymap = get(edge_capacity, G);
		EdgeWeightMap weightmap = get(edge_weight, G);
		ReverseEdgeMap revedgemap = get(edge_reverse, G);
		ResidualCapacityMap rescapacitymap = get(edge_residual_capacity, G);
		EdgeAdder eaG(G, capacitymap, revedgemap);
		
		// setup graph 
		for(int i=0;i<g;i++) eaG.addEdge(sss,i,1);
		for(int i=g;i<g+s;i++) eaG.addEdge(i,ttt,1);
		
		for(int i=0;i<l;i++) {
			int g1,s1; cin>>g1>>s1; 
			eaG.addEdge(g1,g+s1,1);
		}
		
		int flow1 = push_relabel_max_flow(G, sss, ttt);
		// BFS to find vertex set S
		vector<bool> vis(g+s+2, false); // visited flags
		std::queue<int> Q; // BFS queue (from std:: not boost::)
		vis[sss] = true; // Mark the source as visited
		Q.push(sss);
		while (!Q.empty()) {
			const int u = Q.front();
			Q.pop();
			OutEdgeIt ebeg, eend;
			for (tie(ebeg, eend) = out_edges(u, G); ebeg != eend; ++ebeg) {
				const int v = target(*ebeg, G);
				// Only follow edges with spare capacity
				if (rescapacitymap[*ebeg] == 0 || vis[v]) continue;
				vis[v] = true;
				Q.push(v);
			}
		}
		
		vector<int> g_;
		vector<int> s_;
		
		for(int i=0;i<g;i++) if(!vis[i]) g_.push_back(i);
		for(int i=g;i<g+s;i++) if(vis[i]) s_.push_back(i-g);
		
		cout << g_.size() << " ";
		cout << s_.size() << "\n";
		
		if(s_.size() || g_.size()) {
			for(int i=0;i<g_.size();i++) cout << g_[i] << " ";
		
			if(!s_.size()) cout << "\n";
			else {
				for(int i=0;i<s_.size()-1;i++) cout << s_[i] << " ";
				cout << s_[s_.size()-1] << "\n";
			}
		}
	}
    return 0;
}
