// real estate market

// behindert: de edgeAdder im letschte bispel het kei weights...
// 
// trick learnt: successive_shortest_path_nonnegative_weights viel schneller. add constant to negative weights to make them positiv, afterwards remove what is too much

// ALGOLAB BGL Tutorial 3
// Flow example demonstrating
// - breadth first search (BFS) on the residual graph

// Compile and run with one of the following:
// g++ -std=c++11 -O2 bgl_residual_bfs.cpp -o bgl_residual_bfs ./bgl_residual_bfs
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 bgl_residual_bfs.cpp -o bgl_residual_bfs; ./bgl_residual_bfs

// Includes
// ========
// STL includes
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/find_flow_cost.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/cycle_canceling.hpp>
// Namespaces
using namespace std;
using namespace boost;

// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > Graph;
// Interior Property Maps
typedef	property_map<Graph, edge_capacity_t>::type		EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type       EdgeWeightMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type		ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	graph_traits<Graph>::edge_descriptor			Edge;
typedef	graph_traits<Graph>::out_edge_iterator			OutEdgeIt;

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, reverseE;
        tie(e, tuples::ignore) = add_edge(u, v, G);
        tie(reverseE, tuples::ignore) = add_edge(v, u, G);
        capacitymap[e] = c;
        weightmap[e] = w;
        capacitymap[reverseE] = 0;
        weightmap[reverseE] = -w;
        revedgemap[e] = reverseE; 
        revedgemap[reverseE] = e; 
    }
};



// Main
int main() {
	ios_base::sync_with_stdio(false);
	int t; cin>>t;
	while(t--) {
		int N,M,S;cin>>N>>M>>S;
		
		int sss = S+M+N;
		int ttt = S+M+N+1;
		
		// build Graph
		Graph G(S+M+N+2);
		EdgeCapacityMap capacitymap = get(edge_capacity, G);
		EdgeWeightMap weightmap = get(edge_weight, G);
		ReverseEdgeMap revedgemap = get(edge_reverse, G);
		ResidualCapacityMap rescapacitymap = get(edge_residual_capacity, G);
		EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);
		
		
		for(int i=0;i<S;i++) {
			int li; cin>>li;
			eaG.addEdge(sss,i,li,0);
		}
		
		for(int i=0;i<M;i++) {
			int si; cin>>si;
			eaG.addEdge(si-1,S+i,1,0);
		}
		
		for(int i=0;i<N;i++) {
			for(int j=0;j<M;j++) {
				int bij; cin>>bij;
				eaG.addEdge(S+j,S+M+i,1,-bij+100); 
			}
		}
		
		for(int i=0;i<N;i++) {
			eaG.addEdge(S+M+i,ttt,1,0);
		}
		
		/*int flow1 = push_relabel_max_flow(G, sss, ttt);
		cycle_canceling(G);
		int cost1 = find_flow_cost(G);
		cout << flow1 << " " << -cost1 << "\n";*/
		boost::successive_shortest_path_nonnegative_weights(G, sss, ttt);
		int cost2 = boost::find_flow_cost(G);	
		OutEdgeIt e, eend;
		int flow2 = 0;
		for(boost::tie(e, eend) = boost::out_edges(boost::vertex(sss,G), G); e != eend; ++e) {
			flow2 += capacitymap[*e] - rescapacitymap[*e];
		}
        cout << flow2 << " " << -(cost2-100*flow2) << "\n";
	}
	return 0;
}
