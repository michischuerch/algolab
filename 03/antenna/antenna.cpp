#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <iostream>

// typedefs
typedef  CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;
typedef  CGAL::Min_circle_2_traits_2<K>  Traits;
typedef  CGAL::Min_circle_2<Traits>      Min_circle;
typedef K::Point_2 P;

double ceil_to_double(const K::FT& x)
{
 double a = std::floor(CGAL::to_double(x));
 while (a >= x) a -= 1;
 while (a+1 < x) a += 1;
 return a + 1;
}

using namespace std;

int main()
{
	ios_base::sync_with_stdio(false);
    int n;
    cin >> n;
    while(n) {
		long a,b;
		vector<P> data(n);
		for(int i=0;i<n;i++) {
			cin >> a >> b;
			data[i] = P(a,b);
		}
		
		vector<P> result;
	  
		Min_circle mc(result.begin(), result.end(), true);
		Traits::Circle c = mc.circle();
		
		long radius = (long) ceil_to_double(sqrt(c.squared_radius()));
		cout << radius << "\n";
		
		cin >> n;
	}
	return 0;
}
