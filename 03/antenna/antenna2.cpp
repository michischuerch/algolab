#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <CGAL/convex_hull_2.h>


using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Circle_2 C;

double floor_to_double(const K::FT& x)
{
 double a = std::floor(CGAL::to_double(x));
 while (a > x) a -= 1;
 while (a+1 <= x) a += 1;
 return a;
}

int main() {
	int n;
	cin >> n;
	while(n) {
		// data
		vector<P> data(n);
		vector<P> result;
		for(int i=0;i<n;i++) cin >> data[i];
		
		// compute convex hull
		CGAL::convex_hull_2(data.begin(),data.end(),back_inserter(result));
		
		//std::cout << result.size() << " points on the convex hull" << std::endl;
		
		
		
		
		/*data = { P(0,0), P(10,0), P(10,10), P(6,5), P(4,1) };
		//P result[n];
		P *ptr = CGAL::convex_hull_2( data.begin(), data.end(), back_inserter(result) );
		/*cout <<  ptr - result.begin() << " points on the convex hull:" << endl;
		for(int i = 0; i < ptr - result.begin(); i++){
			cout << result[i] << endl;
		}*/
		
		
		
		/*K::FT distance = CGAL::squared_distance(data[0],data[1]);
		for(int i=2;i<n;i++) {
			K::FT d1 = CGAL::squared_distance(data[i],p1);
			K::FT d2 = CGAL::squared_distance(data[i],p2);
			
			if(distance > d2 && d1 > d2) {
				p2 = data[i];
				distance = d1;
			}
			else if(distance > d1 && d2 > d1) {
				p1 = data[i];
				distance = d2;
			}
		}*/
		//cout << sqrt(floor_to_double(distance/2)) << "\n";
		cin >> n;
	}
	return 0;
}
