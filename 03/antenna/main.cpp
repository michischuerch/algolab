#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <iostream>

// typedefs
typedef  CGAL::Exact_predicates_exact_constructions_kernel K;
typedef  CGAL::Min_circle_2_traits_2<K>  Traits;
typedef  CGAL::Min_circle_2<Traits>      Min_circle;
typedef K::Point_2 P;

double floor_to_double(const K::FT& x)
{
 double a = std::floor(CGAL::to_double(x));
 while (a > x) a -= 1;
 while (a+1 <= x) a += 1;
 return a;
}

int main()
{
    int n;
    cin >> n;
    while(n) {
		vector<P> data(n);
		for(int i=0;i<n;i++) cin >> data[i];
	  
		Min_circle mc(data.begin(), data.end(), true);
		Traits::Circle c = mc.circle();
		cout << floor_to_double(c.squared_radius()) << std::endl;
	}
}
