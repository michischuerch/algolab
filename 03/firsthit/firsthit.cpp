#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <iostream>
#include <stdexcept>
#include <limits>
#include <cmath>
#include <vector>
#include <utility>
#include <algorithm>

typedef CGAL::Exact_predicates_exact_constructions_kernel EK;
typedef CGAL::Exact_predicates_exact_constructions_kernel IK;
typedef EK::Point_2 EP;
typedef EK::Segment_2 ES;
typedef EK::Ray_2 ER;

typedef IK::Point_2 IP;
typedef IK::Line_2 IL;
typedef IK::Segment_2 IS;
typedef IK::Ray_2 IR;

using namespace std;

double floor_to_double(const EK::FT& x)
{
	double a = floor(CGAL::to_double(x));
	while (a > x) a -= 1;
	while (a+1 <= x) a += 1;
	return a;
}

bool check_interesting(const EK::FT& d1 , const EK::FT& d2) {
	
	if(d1 > d2) return true;
	
	/*auto o = CGAL::orientation(line1,line2,seg1);
	if(o == CGAL::COLLINEAR) return true;
	auto o2 = CGAL::orientation(line1,line2,seg2);
	if(o2 == CGAL::COLLINEAR) return true;
	else if(o == CGAL::LEFT_TURN && o2 == CGAL::RIGHT_TURN) return true;
	else if(o2 == CGAL::LEFT_TURN && o == CGAL::RIGHT_TURN) return true;*/
	
	
	/*if(CGAL::left_turn(line1,line2,seg1) && CGAL::right_turn(line1,line2,seg2)) return true;
	else if(CGAL::right_turn(line1,line2,seg1) && CGAL::left_turn(line1,line2,seg2)) return true;
	else if(CGAL::collinear(line1,line2,seg1)) return true;
	else if(CGAL::collinear(line1,line2,seg2)) return true;*/
	
	
	return false;
}

int main() {
	while(true) {
		// data
		int n;
		cin >> n;
		if(!n) break;
		long x,y,a,b;
		cin >> x >> y >> a >> b;
		
		EP p(x,y), q(a,b);
		ER phileas(p,q);
		
		vector<pair<EK::FT,int> > dist_seg;
		vector<ES> segments;
		for(int i=0;i<n;i++) {
			long r,s,t,u;
			cin >> r >> s >> t >> u;
			
			ES seg(EP(r,s),EP(t,u));
			dist_seg.push_back(make_pair(CGAL::squared_distance(p,seg),i));
			segments.push_back(seg);
		}
		sort(dist_seg.begin(),dist_seg.end());
		// get the segments and check if the ray intersects
		bool intersect = false;
		EK::FT squared_bestDistance(-1);
		EP bestPoint;
		for(int i=0;i<n;i++) {
			ES seg = segments[dist_seg[i].second];
			EK::FT startDist = dist_seg[i].first;
			if(intersect && check_interesting(startDist,squared_bestDistance)) break;
			
			if (CGAL::do_intersect(phileas,seg)) {
				auto o = CGAL::intersection(phileas,seg);
				if (const EP* op = boost::get<EP>(&*o)) {
					EK::FT d = CGAL::squared_distance(*op,p);
					if(!intersect) {
						squared_bestDistance = d;
						bestPoint = *op;
					}
					else if(squared_bestDistance > d) {
						squared_bestDistance = d;
						bestPoint = *op;
					}
				}
				else if (const ES* os = boost::get<ES>(&*o)) {
					EK::FT d = CGAL::squared_distance(os->source(),p);
					if(!intersect) {
						squared_bestDistance = d;
						bestPoint = os->source();
					}
					else if(squared_bestDistance > d) {
						squared_bestDistance = d;
						bestPoint = os->source();
					}
					d = CGAL::squared_distance(os->target(),p);
					if(squared_bestDistance > d) {
						squared_bestDistance = d;
						bestPoint = os->target();
					}
				}
				else { // how could this be? -> error
					throw std::runtime_error("strange segment intersection");
				}
				intersect = true;
			}		
		}
		long solutionX = floor_to_double(bestPoint.x());
		long solutionY = floor_to_double(bestPoint.y());
		intersect ? cout << solutionX << " " << solutionY << "\n" : cout << "no\n";
	}
	return 0;
}
