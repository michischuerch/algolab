#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <stdexcept>

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Segment_2 S;
typedef K::Ray_2 R;

using namespace std;

int main() {
	while(true) {
		// data
		int n;
		cin >> n;
		if(!n) break;
		long x,y,a,b;
		cin >> x >> y >> a >> b;
		P p(x,y), q(a,b);
		R phileas(p,q);
		// get the segments and check if the ray intersects
		bool intersect = false;
		for(int i=0;i<n;i++) {
			long r,s,t,u;
			cin >> r >> s >> t >> u;
			P w(r,s),v(t,u);
			if (CGAL::do_intersect(phileas,S(w,v))) intersect = true;
			while(intersect && i < n-1) {
				cin >> r >> s >> t >> u;
				i++;
			}
		}
		intersect ? cout << "yes\n" : cout << "no\n";
	}
	return 0;
}
