#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <iostream>
#include <CGAL/convex_hull_2.h>

// typedefs
typedef  CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;
typedef  CGAL::Min_circle_2_traits_2<K>  Traits;
typedef  CGAL::Min_circle_2<Traits>      Min_circle;
typedef K::Point_2 P;

double ceil_to_double(const K::FT& x)
{
 double a = std::floor(CGAL::to_double(x));
 while (a >= x) a -= 1;
 while (a+1 < x) a += 1;
 return a + 1;
}

using namespace std;

int main()
{
	ios_base::sync_with_stdio(false);
    int n;
    cin >> n;
    while(n) {
		long a,b;
		vector<P> data(n);
		for(int i=0;i<n;i++) {
			cin >> a >> b;
			data[i] = P(a,b);
		}
		
		vector<P> result;
  		// compute convex hull
  		//CGAL::convex_hull_2(data.begin(),data.end(),back_inserter(result));
		
		
		Min_circle mc1(data.begin(), data.end(), true);
		Traits::Circle c1 = mc1.circle();
		
		K::FT smallestRadius = c1.squared_radius();
		
		for(int i=0;i<mc1.number_of_support_points();i++) {
		
			vector<P> leaveOneOut;
			for(int j=0;j<data.size();j++) {
				if(data[j] != mc1.support_point(i)) leaveOneOut.push_back(data[j]);
			}
			
			Min_circle mc(leaveOneOut.begin(), leaveOneOut.end(), true);
			Traits::Circle c = mc.circle();
		
			if(c.squared_radius() < smallestRadius) smallestRadius = c.squared_radius();
		}
		long radius = (long long) ceil_to_double(sqrt(smallestRadius));
		cout << radius << "\n";
		
		cin >> n;
	}
	return 0;
}
