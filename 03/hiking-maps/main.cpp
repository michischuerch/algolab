// hiking maps

// pretty dumb exercise where we shouldnt construct the triangles explicitly... -> use that all the points should be on one side of each boundary f the triangle (given by the exercise)
// also very important to minimize the amount of left_turn / right_turn
// -> inexact_construction_kernel!

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <CGAL/Triangle_2.h>
#include <utility>
#include <climits>
#include <algorithm>

// i forgot the inexact kernel, -> always use this if possible...
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Segment_2 S;
typedef K::Line_2 L;
typedef K::Triangle_2 T;

using namespace std;

int pointNumber = 0;

P getIntersection(L line1, L line2) {
	P result = P(0,0);
	if (CGAL::do_intersect(line1,line2)) {
		auto o = CGAL::intersection(line1,line2);
			if (const P* op = boost::get<P>(&*o))
				result = *op;
			else // how could this be? -> error
			  throw std::runtime_error("strange segment intersection");
		  } else
			std::cout << "no intersection\n";
	return result;
}

bool checkCovered(int i, int j, vector<vector<int> > &p, vector<vector<int> > &s, vector<T> &t, vector<P> &h) {
	
	// if we dont know about the segment
	// we need to calculate if the points of the segment are in the triangle and update accordingly
	if(s[i][j] == -1) {
	
		if(p[i][j] == -1) {
			CGAL::Bounded_side bs = t[i].bounded_side(h[j]);
			if(bs == CGAL::ON_BOUNDARY || bs == CGAL::ON_BOUNDED_SIDE) p[i][j] = 1;
			else p[i][j] = 0;
			pointNumber++;
		}
		
		if(p[i][j+1] == -1) {
			CGAL::Bounded_side bs = t[i].bounded_side(h[j+1]);
			if(bs == CGAL::ON_BOUNDARY || bs == CGAL::ON_BOUNDED_SIDE) p[i][j+1] = 1;
			else p[i][j+1] = 0;
			pointNumber++;
		}  
		
		s[i][j] = p[i][j] && p[i][j+1];
	}
	
	return s[i][j];
				
}

int main()
{
	// data
	int c;
	cin >> c;
	
	while(c--) {
		int n,m;
		cin >> m >> n;
		
		vector<P> hiking;
		
		for(int i=0;i<m;i++) {
			int x,y;
			cin >> x >> y;
			hiking.push_back(P(x,y));
		}
		
		vector<vector<P>> triangles;
		vector<vector<P>> checkPoints;
		
		for(int i=0;i<n;i++) {
			int a1,a2,a3,a4,b1,b2,b3,b4,c1,c2,c3,c4;
			cin >> a1 >> a2 >> a3 >> a4 >> b1 >> b2 >> b3 >> b4 >> c1 >> c2 >> c3 >> c4;
			// build triangle points
			vector<P> lines={P(a1,a2),P(a3,a4),P(b1,b2),P(b3,b4),P(c1,c2),P(c3,c4)};
			vector<P> points={P(b1,b2),P(c1,c2),P(a1,a2)};
			triangles.push_back(lines);
			checkPoints.push_back(points);
		}
		
		vector<pair<int,int>> searchSnippets;
		for(int i=0;i<triangles.size();i++) {
			vector<bool> pc(m,false);
			
			for(int k=0;k<3;k++) {
				bool leftTurn = CGAL::right_turn(triangles[i][2*k],triangles[i][2*k+1],checkPoints[i][k]);
				if(leftTurn) swap(triangles[i][2*k],triangles[i][2*k+1]);
			}
			for(int j=0;j<m;j++) {
				// i took this from the solutions, this minimizes the amount of right_turns 
				if(!CGAL::right_turn(triangles[i][0],triangles[i][1],hiking[j]) && 
					!CGAL::right_turn(triangles[i][2],triangles[i][3],hiking[j]) && 
					!CGAL::right_turn(triangles[i][4],triangles[i][5],hiking[j])) pc[j] = true;
				if(j>0 && pc[j] && pc[j-1]) searchSnippets.push_back(make_pair(i,j-1));
			}
		}
		sort(searchSnippets.begin(),searchSnippets.end());
		
		// perform search snippets...
		int max = searchSnippets.back().first;
		int min = searchSnippets.front().first;
		int solution = max-min;
		vector<int> p(m-1,0);
		int c = 0;
		int pointer = 0;
		for(int i=0;i<searchSnippets.size();i++) {
			if(p[searchSnippets[i].second]==0) c++;
			p[searchSnippets[i].second]++;
			while(p[searchSnippets[pointer].second] > 1) {
				p[searchSnippets[pointer].second]--;
				pointer++;
			}
			if(c==m-1) {
				max=searchSnippets[i].first;
				min=searchSnippets[pointer].first;
			}
			if(solution>max-min) solution = max-min;
		}
		cout << solution+1 << "\n";
	}
	
	return 0;
}
