#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <CGAL/Triangle_2.h>
#include <utility>

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Segment_2 S;
typedef K::Line_2 L;
typedef K::Triangle_2 T;

using namespace std;

int pointNumber = 0;

P getIntersection(L line1, L line2) {
	P result = P(0,0);
	if (CGAL::do_intersect(line1,line2)) {
		auto o = CGAL::intersection(line1,line2);
			if (const P* op = boost::get<P>(&*o))
				result = *op;
			else // how could this be? -> error
			  throw std::runtime_error("strange segment intersection");
		  } else
			std::cout << "no intersection\n";
	return result;
}

bool checkCovered(int i, int j, vector<vector<int> > &p, vector<vector<int> > &s, vector<T> &t, vector<P> &h) {
	
	// if we dont know about the segment
	// we need to calculate if the points of the segment are in the triangle and update accordingly
	if(s[i][j] == -1) {
	
		if(p[i][j] == -1) {
			CGAL::Bounded_side bs = t[i].bounded_side(h[j]);
			if(bs == CGAL::ON_BOUNDARY || bs == CGAL::ON_BOUNDED_SIDE) p[i][j] = 1;
			else p[i][j] = 0;
			pointNumber++;
		}
		
		if(p[i][j+1] == -1) {
			CGAL::Bounded_side bs = t[i].bounded_side(h[j+1]);
			if(bs == CGAL::ON_BOUNDARY || bs == CGAL::ON_BOUNDED_SIDE) p[i][j+1] = 1;
			else p[i][j+1] = 0;
			pointNumber++;
		}  
		
		s[i][j] = p[i][j] && p[i][j+1];
	}
	
	return s[i][j];
				
}

int main()
{
	// data
	int c;
	cin >> c;
	
	while(c--) {
		int n,m;
		cin >> m >> n;
		
		vector<P> hiking;
		
		for(int i=0;i<m;i++) {
			int x,y;
			cin >> x >> y;
			hiking.push_back(P(x,y));
		}
		
		vector<T> triangles;
		
		for(int i=0;i<n;i++) {
			int a1,a2,a3,a4,b1,b2,b3,b4,c1,c2,c3,c4;
			cin >> a1 >> a2 >> a3 >> a4 >> b1 >> b2 >> b3 >> b4 >> c1 >> c2 >> c3 >> c4;
			// build triangle points
			L line1 = L(P(a1,a2),P(a3,a4));
			L line2 = L(P(b1,b2),P(b3,b4));
			L line3 = L(P(c1,c2),P(c3,c4));
			triangles.push_back(T(getIntersection(line1,line2),getIntersection(line2,line3),getIntersection(line3,line1)));
		}
		
		/*vector<vector<bool> > segCovered;
		vector<int> numSegCovered(n,0);
		for(int i=0;i<triangles.size();i++) {
			vector<bool> pc(m,false);
			vector<bool> s(m-1,false);
			for(int j=0;j<m;j++) {
				
				CGAL::Bounded_side bs = triangles[i].bounded_side(hiking[j]);
				if(bs == CGAL::ON_BOUNDARY || bs == CGAL::ON_BOUNDED_SIDE) pc[j] = true;
				
				
				if(j>0 && pc[j] && pc[j-1]) {
					s[j-1]=true;
					numSegCovered[i]++;
				}
			}
			segCovered.push_back(s);
		}*/
		
		// -1 = dont know, 0 = no, 1 = yes
		vector<vector<int> > pointsCovered;
		vector<vector<int> > segsCovered;
		for(int i=0;i<n;i++) {
			vector<int> v1(m,-1);
			pointsCovered.push_back(v1);
			vector<int> v2(m-1,-1);
			segsCovered.push_back(v2);
		}
		
		int bestValue = n;
		int left = 0, right = 0;
		
		vector<int> permutation;
		for(int i=0;i<m-1;i++) permutation.push_back(i);
		random_shuffle(permutation.begin(), permutation.end());
		
		while(left < n) {
			//cout << "(" << left << "," << right << ") b(" << bestValue << ") \n";
			for(int j=0;j<m-1 && right < n;j++) {
				int nextJ = j;//permutation[j];
				if(checkCovered(right, nextJ, pointsCovered, segsCovered, triangles, hiking)) {
					/*for(int k=left;k<right-1;k++) {
						if(segsCovered[k][nextJ] == -1) segsCovered.at(k).at(nextJ) = 1;
					}*/
				} else {
					bool isIn = false;
					for(int k=right-1;k>=left;k--) {
						if(checkCovered(k, nextJ, pointsCovered, segsCovered, triangles, hiking)) {
							isIn = true;
							break;
							/*while(k>=left) {
								segsCovered.at(k).at(nextJ) = 1;
								k--;
							}*/
						}
					}
					if(!isIn) {
						right++;
						j--;
					}
				}
			}
			
			
			
			if(right >= n) break;
			
			int bestLeft = right;
			for(int j=0;j<m-1;j++) {
				int nextJ = j;//permutation[j];
				if(segsCovered[right][nextJ] == 1) {
					for(int i=bestLeft;i>left;i--) {
						if(checkCovered(i, nextJ, pointsCovered, segsCovered, triangles, hiking)) {
							bestLeft = min(i,bestLeft);
							break;
						}
					}
				} else break;
			}
			
			/*for(int j=0;j<m-1;j++) {
				if(checkCovered(right, j, pointsCovered, segsCovered, triangles, hiking)) {
					for(int k=left;k<right-1;k++) {
						if(segsCovered[k][j] == -1) segsCovered.at(k).at(j) = 1;
					}
				}
			}*/
			
			
			// now right is the last element of the first valid sequence. 
			bestValue = min(bestValue,right-left+1);
			//left = max(max(left+1,right-bestValue+1),bestLeft);
			left = max(left+1,right-bestValue+1);
			right = left;
		}
			/*vector<bool> s = segCovered[i];
			int numSegs = numSegCovered[i];
			int value = 1;
			if(numSegs == m-1) bestValue = 1;
			for(int ii=i+1;ii<n;ii++) {
				
				if(ii - i >= bestValue) {
					value = bestValue;
					break;
				}
				
				vector<bool> ss = segCovered[ii];
				for(int j=0;j<m;j++) {
					if(!s[j] && ss[j]) {
						s[j] = true;
						numSegs++;
					}
				}
				if(numSegs == m-1) {
					value = ii - i + 1;
					break;
				}
			}
			
			if(numSegs == m-1 && bestValue > value) bestValue = value;*/
			
		//}
		//cout << "worst case, ours: " << m*n << "," << pointNumber << " ";
		pointNumber=0;
		cout << bestValue << "\n";
		
		
	}
	
	return 0;
}
